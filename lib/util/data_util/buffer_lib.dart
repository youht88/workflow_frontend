import 'dart:convert';
import 'dart:typed_data';

import 'package:fast_base58/fast_base58.dart';

class BufferLib {
  static String bufferToHexString(Uint8List code, [bool prefix0x = false]) {
    String hexCode =
        code.map((e) => e.toRadixString(16).padLeft(2, '0')).join();
    if (prefix0x) {
      return '0x$hexCode';
    } else {
      return hexCode;
    }
  }

  static List<int> hexStringToBuffer(String str, [bool prefix0x = false]) {
    if (prefix0x) {
      str = str.substring(2);
    }
    RegExp reg = RegExp("[a-f0-9A-F][a-f0-9A-F]");
    List<int> res = reg
        .allMatches(str)
        .map((item) => int.parse("${item.group(0)}", radix: 16))
        .toList();
    return res;
  }

  static String base64ToHexString(String base64str) {
    return base64Decode(base64str)
        .map((e) => e.toRadixString(16).padLeft(2, '0'))
        .join();
  }

  static dynamic myEncode(dynamic item) {
    if (item is DateTime) {
      return item.toIso8601String();
    }
    return item;
  }

  //对任何基本数据编码成base64用于传输
  static String encode58(String data) {
    return Base58Encode(utf8.encode(data) as Uint8List);
  }

  //解码base64编码的数据
  static String decode58(String data) {
    return utf8.decode(Base58Decode(data));
  }

  //对任何基本数据编码成base64用于传输
  static String encode(data) {
    return base64Encode(utf8.encode(json.encode(data, toEncodable: myEncode)));
  }

  //解码base64编码的数据
  static dynamic decode(String data) {
    return json.decode(utf8.decode(base64Decode(data)));
  }

  //带自动解析DateTime
  static String jsonEncode(data) {
    return json.encode(data, toEncodable: myEncode);
  }

  static dynamic jsonDecode(data) {
    return json.decode(data);
  }

  static String asciiListToBuffer(List<int> asciiList) {
    final asciiDecoder = AsciiDecoder(allowInvalid: true);
    print(asciiDecoder.convert(asciiList));
    print(utf8.encode(asciiDecoder.convert(asciiList)));
    return BufferLib.bufferToHexString(
        Uint8List.fromList(utf8.encode(asciiDecoder.convert(asciiList))));
  }
}
