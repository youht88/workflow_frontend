import 'package:intl/intl.dart';

import 'string_lib.dart';

class DateTimeLib {
  static DateTime now() {
    return DateTime.now();
  }

  static String todayStr() {
    return DateTimeLib.format(DateTime.now(), "YYYYMMDD");
  }

  static String thisMonthStr() {
    return DateTimeLib.format(DateTime.now(), "YYYYMM");
  }

  static String thisYearStr() {
    return DateTimeLib.format(DateTime.now(), "YYYY");
  }

  static bool isToday(dynamic date) {
    String todayStr = DateTimeLib.todayStr();
    if (date is String) {
      var temp = DateTimeLib.fromStr(date);
      if (temp == null) return false;
      return DateTimeLib.format(temp, "YYYYMMDD") == todayStr;
    }
    if (date is DateTime) {
      return DateTimeLib.format(date, "YYYYMMDD") == todayStr;
    }
    return false;
  }

  /// 将字符串转换为日期型
  /// * 允许字符串是fromMicrosecondsSinceEpoch
  /// * 允许字符串是YYYYMMDD
  /// * 允许字符串是yyyy.mm.dd hh:mi:ss
  /// * 允许字符串是yyyy-mm-dd hh:mi:ss
  /// * 允许字符串是yyyy/mm/dd hh:mi:ss
  /// *
  static DateTime? fromStr(String str) {
    DateTime? res;
    try {
      if (str.length == 8) {
        res = DateTime(int.parse(str.substring(0, 4)),
            int.parse(str.substring(4, 6)), int.parse(str.substring(6, 8)));
      } else if (str.length >= 10 && str[10] == "T") {
        res = DateTime.parse(str);
      } else {
        res = DateTime.parse(
            str.substring(0, 10).replaceAll(".", "-") + str.substring(10));
      }
    } catch (e) {
      int? epoch = int.tryParse(str);
      if (epoch != null) res = DateTime.fromMicrosecondsSinceEpoch(epoch);
    }
    return res;
  }

  /// 获取指定日期之间的所有日期
  /// 接受日期型参数和“YYYYMMDD”格式的字符串
  static List<String> getDaysBetween(dynamic start, dynamic end) {
    DateTime s, e;
    if (start is String) {
      s = DateTime(int.parse(start.substring(0, 4)),
          int.parse(start.substring(4, 6)), int.parse(start.substring(6, 8)));
    } else {
      s = start;
    }
    if (end is String) {
      e = DateTime(int.parse(end.substring(0, 4)),
          int.parse(end.substring(4, 6)), int.parse(end.substring(6, 8)));
    } else {
      e = end;
    }
    List<String> days = [];
    do {
      days.add(DateTimeLib.format(s, "YYYYMMDD"));
      s = s.add(Duration(days: 1));
      if (s.isAfter(e)) break;
    } while (true);
    return days;
  }

  /// 获取指定月份之间的所有月份
  /// /// 接受日期型参数和“YYYYMM”格式的字符串
  static List<String> getMonthsBetween(start, end) {
    DateTime s, e;
    if (start is String) {
      s = DateTime(
          int.parse(start.substring(0, 4)), int.parse(start.substring(4, 6)));
    } else {
      s = start;
    }
    if (end is String) {
      e = DateTime(
          int.parse(end.substring(0, 4)), int.parse(end.substring(4, 6)));
    } else {
      e = end;
    }
    List<String> months = [];
    do {
      months.add(DateTimeLib.format(s, "YYYYMM"));
      s = DateTimeLib.monthLastDay(s).add(Duration(days: 1));
      if (s.isAfter(e)) break;
    } while (true);
    return months;
  }

  /// 获得time所在周的日期字串
  ///
  /// 返回如:["20211107","20211108",...,"20211113"]
  static List<String> getWeekDays(DateTime time) {
    DateTime start = DateTimeLib.weekFirstDay(time);
    DateTime end = DateTimeLib.weekLastDay(time);
    DateTime currentDay = start;
    DateTime endDay = end.add(Duration(days: 1));
    //区间数据准备
    List<String> res = [];
    while (currentDay.isBefore(endDay)) {
      res.add(DateTimeLib.format(currentDay, "YYYYMMDD"));
      currentDay = currentDay.add(Duration(days: 1));
    }
    return res;
  }

  ///获取两段时间的时间间隔
  static Duration getDurationByStr(String startTime, String endTime) {
    DateTime start = DateTime.parse(startTime);
    DateTime end = DateTime.parse(endTime);
    return end.difference(start);
  }

  /// 获得time所在月的日期字串
  ///
  /// 返回如:["20210301","20210302",...,"20210331"]
  static List<String> getMonthDays(DateTime time) {
    DateTime start = DateTimeLib.monthFirstDay(time);
    DateTime end = DateTimeLib.monthLastDay(time);
    DateTime currentDay = start;
    DateTime endDay = end.add(Duration(days: 1));
    //区间数据准备
    List<String> res = [];
    while (currentDay.isBefore(endDay)) {
      res.add(DateTimeLib.format(currentDay, "YYYYMMDD"));
      currentDay = currentDay.add(Duration(days: 1));
    }
    return res;
  }

  /// 获得time所在年的日期字串
  ///
  /// 返回如:["20210101","20210102",...,"20211231"]
  static List<String> getYearDays(DateTime time) {
    DateTime start = DateTimeLib.yearFirstDay(time);
    DateTime end = DateTimeLib.yearLastDay(time);
    DateTime currentDay = start;
    DateTime endDay = end.add(Duration(days: 1));
    //区间数据准备
    List<String> res = [];
    while (currentDay.isBefore(endDay)) {
      res.add(DateTimeLib.format(currentDay, "YYYYMMDD"));
      currentDay = currentDay.add(Duration(days: 1));
    }
    return res;
  }

  /// 获得time所在年的月份字串
  ///
  /// 返回如:["202101","202102",...,"202112"]
  static List<String> getYearMonths(DateTime time) {
    String year = time.year.toString();
    List<String> res = [
      "${year}01",
      "${year}02",
      "${year}03",
      "${year}04",
      "${year}05",
      "${year}06",
      "${year}07",
      "${year}08",
      "${year}09",
      "${year}10",
      "${year}11",
      "${year}12"
    ];
    return res;
  }

  /// 获得time所在年的季度字串
  ///
  /// 返回如:["2021Q1","2021Q2","2021Q3","2021Q4"]

  static List<String> getYearQuarters(DateTime time) {
    String year = time.year.toString();
    List<String> res = [
      "${year}Q1",
      "${year}Q2",
      "${year}Q3",
      "${year}Q4",
    ];
    return res;
  }

  /// 获得time所在季度的月份字串
  ///
  /// 返回如:["202101","202102","202103"]
  static List<String> getQuarterMonths(DateTime time) {
    int quarter = (time.month - 1) ~/ 3 + 1;

    List<String> res = [
      "${time.year}${((quarter - 1) * 3 + 1).toString().padLeft(2, '0')}",
      "${time.year}${((quarter - 1) * 3 + 2).toString().padLeft(2, '0')}",
      "${time.year}${((quarter - 1) * 3 + 3).toString().padLeft(2, '0')}",
    ];
    return res;
  }

  static DateTime? firstOf(DateTime time, String acc) {
    switch (acc) {
      case "year":
        return yearFirstDay(time);
      case "quarter":
        return quarterFirstDay(time);
      case "month":
        return monthFirstDay(time);
      case "week":
        return weekFirstDay(time);
      case "day":
        return dayFirstHour(time);
      case "hour":
        return DateTime(time.year, time.month, time.day, time.hour, 0, 0);
      case "minute":
        return DateTime(
            time.year, time.month, time.day, time.hour, time.minute, 0);
      default:
        return null;
    }
  }

  static DateTime? lastOf(DateTime time, String acc) {
    switch (acc) {
      case "year":
        return yearLastDay(time);
      case "quarter":
        return quarterLastDay(time);
      case "month":
        return monthLastDay(time);
      case "week":
        return weekLastDay(time);
      case "day":
        return dayLastHour(time);
      case "hour":
        return DateTime(time.year, time.month, time.day, time.hour, 59, 59);
      case "minute":
        return DateTime(
            time.year, time.month, time.day, time.hour, time.minute, 59);
      default:
        return null;
    }
  }

  static DateTime? add(DateTime time, int value, String acc) {
    switch (acc) {
      case "year":
        return DateTime(time.year + value, time.month, time.day, time.hour,
            time.minute, time.second);
      case "quarter":
        return DateTime(time.year, time.month - value * 3, time.day, time.hour,
            time.minute, time.second);
      case "month":
        return DateTime(time.year, time.month + value, time.day, time.hour,
            time.minute, time.second);
      case "week":
        return DateTime(time.year, time.month, time.day + value * 7, time.hour,
            time.minute, time.second);
      case "day":
        return DateTime(time.year, time.month, time.day + value, time.hour,
            time.minute, time.second);
      case "hour":
        return DateTime(time.year, time.month, time.day, time.hour + value,
            time.minute, time.second);
      case "minute":
        return DateTime(time.year, time.month, time.day, time.hour,
            time.minute + value, time.second);
      case "second":
        return DateTime(time.year, time.month, time.day, time.hour, time.minute,
            time.second + value);
    }
  }

  static DateTime? substract(DateTime time, int value, String acc) {
    switch (acc) {
      case "year":
        return DateTime(time.year - value, time.month, time.day, time.hour,
            time.minute, time.second);
      case "quarter":
        return DateTime(time.year, time.month - value * 3, time.day, time.hour,
            time.minute, time.second);
      case "month":
        return DateTime(time.year, time.month - value, time.day, time.hour,
            time.minute, time.second);
      case "week":
        return DateTime(time.year, time.month, time.day - value * 7, time.hour,
            time.minute, time.second);
      case "day":
        return DateTime(time.year, time.month, time.day - value, time.hour,
            time.minute, time.second);
      case "hour":
        return DateTime(time.year, time.month, time.day, time.hour - value,
            time.minute, time.second);
      case "minute":
        return DateTime(time.year, time.month, time.day, time.hour,
            time.minute - value, time.second);
      case "second":
        return DateTime(time.year, time.month, time.day, time.hour, time.minute,
            time.second - value);
    }
  }

  static DateTime dayFirstHour(DateTime time) {
    return DateTime(time.year, time.month, time.day, 0);
  }

  static DateTime dayLastHour(DateTime time) {
    return DateTime(time.year, time.month, time.day, 23, 59, 59);
  }

  static DateTime weekFirstDay(DateTime time) {
    time = DateTime(time.year, time.month, time.day);
    return time.subtract(Duration(days: time.weekday % 7 - 1));
  }

  static DateTime weekLastDay(DateTime time) {
    time = DateTime(time.year, time.month, time.day);
    return time.add(Duration(days: 7 - time.weekday % 7));
  }

  static DateTime quarterFirstDay(DateTime time) {
    int quarter = (time.month - 1) ~/ 3 + 1;
    return DateTime(time.year, (quarter - 1) * 3 + 1, 1);
  }

  static DateTime quarterLastDay(DateTime time) {
    int quarter = (time.month - 1) ~/ 3 + 1;
    DateTime temp = DateTime(time.year, (quarter - 1) * 3 + 3);
    return monthLastDay(temp);
  }

  static DateTime monthFirstDay(DateTime time) {
    return DateTime(time.year, time.month);
  }

  static DateTime monthLastDay(DateTime time) {
    time = DateTime(time.year, time.month + 1);
    return time.subtract(Duration(days: 1));
  }

  static DateTime yearFirstDay(DateTime time) {
    return DateTime(time.year);
  }

  static DateTime yearLastDay(DateTime time) {
    time = DateTime(time.year + 1);
    return time.subtract(Duration(days: 1));
  }

  static String format(DateTime time, String patten) {
    String rtn = patten.toUpperCase();
    rtn = rtn.replaceAll("YYYY", time.year.toString());
    rtn = rtn.replaceAll("MM", time.month.toString().padLeft(2, '0'));
    rtn = rtn.replaceAll("DD", time.day.toString().padLeft(2, '0'));
    rtn = rtn.replaceAll("HH", time.hour.toString().padLeft(2, '0'));
    rtn = rtn.replaceAll("MI", time.minute.toString().padLeft(2, '0'));
    rtn = rtn.replaceAll("SS", time.second.toString().padLeft(2, '0'));
    return rtn;
  }

  //返回当前时间毫秒级
  static int timeStamp() {
    return DateTime.now().millisecondsSinceEpoch;
  }

  //返回当前时间微秒级
  static int microTimeStamp() {
    return DateTime.now().microsecondsSinceEpoch;
  }

  ///获取当前时间的零点时间戳
  static int getStartTime(DateTime dateTime) {
    var format = DateFormat('yyyy-MM-dd');
    String time = format.format(dateTime);
    return DateTime.parse(time).microsecondsSinceEpoch;
  }

  ///获取当前时间的23点59分59秒999
  static int getEndTime(DateTime dateTime) {
    return getStartTime(dateTime) + (24 * 60 * 60 * 1000 - 1) * 1000;
  }

  static String formatDate(DateTime dateTime) {
    final now = DateTime.now();
    final difference = dateTime.difference(now);
    if (difference.inMinutes.abs() < 1) {
      return "刚刚";
    } else if (difference.inMinutes.abs() >= 1 &&
        difference.inMinutes.abs() < 30) {
      //1-30分钟
      return "${difference.inMinutes.abs()}分钟前";
    } else {
      final compare = DateTime(now.year, now.month, now.day, 23, 59, 59, 999);
      final difference = dateTime.difference(compare);
      if (difference.inDays.abs() == 0) {
        //当前
        return DateFormat.Hm().format(dateTime);
      } else if (difference.inDays.abs() == 1) {
        return "昨天 ${DateFormat.Hm().format(dateTime)}";
      } else if (now.year == dateTime.year) {
        //月,日 时,分
        var format = DateFormat('MM-dd HH:mm');
        return format.format(dateTime);
      } else {
        var format = DateFormat('yyyy-MM-dd HH:mm');
        return format.format(dateTime);
      }
    }
  }

  //把字串翻译成日期类型  (还不可靠)
  //today 表示当天
  //<+/-><数值><y/q/M/w/d/h/m/s/> 来表示相对当天的前后若干个时间单位。例如
  // +2d 后两天 ， -3m 前三个月 ， -1y 前一年 +2.5w 两周半以后，等等
  // 注意M表示月，m表示分钟
  static DateTime? tryParseDuration(String str) {
    if (str == 'today') return DateTime.now();
    List match =
        StringLib.regMatch(str, RegExp(r"([\+\-])([\d\.]+)([yqMdhms])"));
    if (match.length == 0) return null;
    num? duration = num.tryParse(match[2]);
    if (duration == null) return null;
    switch (match[3]) {
      case "y":
        if (match[1] == "+")
          return DateTime.now().add(Duration(days: (duration * 365).toInt()));
        return DateTime.now()
            .subtract(Duration(days: (duration * 365).toInt()));
      case "M":
        if (match[1] == "+")
          return DateTime.now().add(Duration(days: (duration * 30).toInt()));
        return DateTime.now().subtract(Duration(days: (duration * 30).toInt()));
      case "q":
        if (match[1] == "+")
          return DateTime.now().add(Duration(days: (duration * 90).toInt()));
        return DateTime.now().subtract(Duration(days: (duration * 90).toInt()));
      case "w":
        if (match[1] == "+")
          return DateTime.now().add(Duration(days: (duration * 7).toInt()));
        return DateTime.now().subtract(Duration(days: (duration * 7).toInt()));
      case "d":
        if (match[1] == "+")
          return DateTime.now().add(Duration(days: duration.toInt()));
        return DateTime.now().subtract(Duration(days: duration.toInt()));
      case "h":
        if (match[1] == "+")
          return DateTime.now().add(Duration(hours: duration.toInt()));
        return DateTime.now().subtract(Duration(days: duration.toInt()));
      case "m":
        if (match[1] == "+")
          return DateTime.now().add(Duration(minutes: duration.toInt()));
        return DateTime.now().subtract(Duration(minutes: duration.toInt()));
      case "s":
        if (match[1] == "+")
          return DateTime.now().add(Duration(seconds: duration.toInt()));
        return DateTime.now().subtract(Duration(seconds: duration.toInt()));
      default:
        return null;
    }
  }
}
