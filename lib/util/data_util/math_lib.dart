import 'dart:math';

import 'package:open_simplex_noise/open_simplex_noise.dart';

class MathLib {
  static Random random = Random();
  static OpenSimplexNoise noise = OpenSimplexNoise();
  static double noise2d(num x, num y) {
    return noise.eval2D(x.toDouble(), y.toDouble());
  }

  static double noise3d(num x, num y, num z) {
    return noise.eval3D(x.toDouble(), y.toDouble(), z.toDouble());
  }

  static double noise4d(num x, num y, num z, num w) {
    return noise.eval4D(x.toDouble(), y.toDouble(), z.toDouble(), w.toDouble());
  }

  static num map(num value, num from, num to, num min, num max) {
    if (value < from) value = from;
    if (value > to) value = to;
    num result = ((max - min) / (to - from) * (value - from)) + min;
    return result;
  }

  static double randomDouble(double start, double end) {
    return random.nextDouble() * (end - start) + start;
  }

  static int randomInt(int start, int end) {
    return random.nextInt(end - start) + start;
  }

  static List<double> randomDoubleList(double start, double end, int n) {
    return List.generate(
        n, (index) => random.nextDouble() * (end - start) + start).toList();
  }

  static List<int> randomIntList(int start, int end, int n) {
    return List.generate(n, (index) => random.nextInt(end - start) + start)
        .toList();
  }

  static double sum(List<double> data) => data.reduce((x, y) => x + y);
  static List<double> factor(List<double> data) {
    final double summary = sum(data);
    return data.map((x) => x / summary).toList();
  }

  static double avg(List<double> data) => sum(data) / data.length;
  static double vari(List<double> data) =>
      data.map((x) => pow(x - avg(data), 2)).reduce((x, y) => x + y) /
      data.length;
  static double std(List<double> data) => sqrt(vari(data));
  static Map<String, double> stat(List<double> data, {bool extra = false}) {
    //计算给定数据集的最小，最大，总计，平均，计数统计量，如果extra==true，还计算方差
    Map<String, double> res = {};
    res["count"] = data.length.toDouble();
    res["sum"] = 0.0;
    res["max"] = data[0];
    res["min"] = data[0];
    for (int i = 0; i < data.length; i++) {
      if (res["max"]! < data[i]) {
        res["max"] = data[i];
      }
      if (res["min"]! > data[i]) {
        res["min"] = data[i];
      }
      res["sum"] = res["sum"]! + data[i];
    }
    res["avg"] = res["sum"]! / data.length;
    if (!extra) {
      return res;
    }
    res["var"] =
        data.map((i) => pow(i - res["avg"]!, 2)).reduce((x, y) => x + y) /
            data.length;
    res["std"] = sqrt(res["var"]!);
    return res;
  }

  ///key如果为空 默认采用英文自动转换
  ///
  ///key=en 采用英文转换
  ///
  ///key=cn 采用中文转换
  ///
  ///key = "M" 或 "亿"这种指定单位，则转换到该单位
  static String formater(double number, int digits, [String? key]) {
    Map<String, num> enUnit = {
      "": 1,
      "k": 1e3,
      "M": 1e6,
      "G": 1e9,
      "T": 1e12,
      "P": 1e15,
      "E": 1e18,
      "Z": 1e21,
    };
    Map<String, num> cnUnit = {
      "": 1,
      "千": 1e3,
      "万": 1e4,
      "亿": 1e8,
      "兆": 1e12,
      "京": 1e16,
    };
    var unit = {};
    var rx = RegExp(r'.0+$|(.[0-9]*[1-9])0+$');
    var unitKey = "";

    if (enUnit.keys.contains(key)) {
      return (number / enUnit[key]!)
              .toStringAsFixed(digits)
              .replaceAll(rx, '') +
          key!;
    } else if (cnUnit.keys.contains(key)) {
      return (number / cnUnit[key]!)
              .toStringAsFixed(digits)
              .replaceAll(rx, '') +
          key!;
    }

    if (key == null || key == "en") {
      unit = enUnit;
    } else if (key == "cn") {
      unit = cnUnit;
    }
    var keys = unit.keys.toList();
    for (int i = 0; i < keys.length; i++) {
      if (number.abs() < unit[keys[i]]!) {
        unitKey = keys[i];
        break;
      }
    }
    return number.toStringAsFixed(digits).replaceAll(rx, '') + unitKey;
    // return (number / unit[unitKey]!)
    //         .toStringAsFixed(digits)
    //         .replaceAll(rx, '') +
    //     unitKey;
  }
}
