import 'dart:convert';
import 'dart:typed_data';

import 'package:pointycastle/export.dart';
import 'package:workflow/util/data_util/data_util.dart';

class HashLib {
  static String md5(dynamic msg) {
    Uint8List result;
    if (msg is String) {
      result = MD5Digest().process(Uint8List.fromList(utf8.encode(msg)));
    } else {
      result = MD5Digest().process(Uint8List.fromList(msg));
    }
    return BufferLib.bufferToHexString(result);
  }

  static String ripemd160(dynamic msg) {
    Uint8List result;
    if (msg is String) {
      result = RIPEMD160Digest().process(Uint8List.fromList(utf8.encode(msg)));
    } else {
      result = RIPEMD160Digest().process(Uint8List.fromList(msg));
    }
    return BufferLib.bufferToHexString(result);
  }

  static String sha256(dynamic msg) {
    Uint8List result;
    if (msg is String) {
      result = SHA256Digest().process(Uint8List.fromList(utf8.encode(msg)));
    } else {
      result = SHA256Digest().process(Uint8List.fromList(msg));
    }
    return BufferLib.bufferToHexString(result);
  }

  static String sha512(dynamic msg) {
    Uint8List result;
    if (msg is String) {
      result = SHA512Digest().process(Uint8List.fromList(utf8.encode(msg)));
    } else {
      result = SHA512Digest().process(Uint8List.fromList(msg));
    }
    return BufferLib.bufferToHexString(result);
  }

  static String sm3(dynamic msg) {
    Uint8List result;
    if (msg is String) {
      result = SM3Digest().process(Uint8List.fromList(utf8.encode(msg)));
    } else {
      result = SM3Digest().process(Uint8List.fromList(msg));
    }
    return BufferLib.bufferToHexString(result);
  }

  static String keccak256(dynamic msg) {
    Uint8List result;
    if (msg is String) {
      result = KeccakDigest(256).process(Uint8List.fromList(utf8.encode(msg)));
    } else {
      result = KeccakDigest(256).process(Uint8List.fromList(msg));
    }
    return BufferLib.bufferToHexString(result);
  }
}
