import 'dart:async';
import 'dart:async' as ac;
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:workflow/data/constant.dart';
import 'package:workflow/util/crypto_util/crypto_util.dart';
import 'package:workflow/util/data_util/buffer_lib.dart';
import 'package:workflow/util/file/file_util.dart';
import 'package:workflow/util/icfs/icfs_lib.dart';
import 'package:workflow/util/js/js_util.dart';
import 'package:workflow/util/network/http_util.dart';

/// Storage 封装GetStroage大部分实用功能
///
/// Storage存储int，double，String,bool,List，Map。但请避免存储DateTime类型
///
/// ### `重要提示` 使用前必须先执行Storage.init()
/// ### 具体函数说明
///
/// * set函数是一个async函数，在设置memory的同时也会存储到disk中，
/// 如果调用setInMemory则不会自动存储到disk中，可以通过异步的save函数落盘。
/// * get函数是同步函数，直接从内存中提取数据，例如
/// ``` dart
///     await Storage.set("key","value");
///     String value = Storage.get("key");
///     Stroage.setInMemory("key1",123);
///     Storage.setInMemory("key2","abc");
///     Storage.save();
/// ```
/// * `has(key)` 判断key是否存在
/// * `remove(key)` 删除某个key的值
/// * `reset()` 清空整个存储
/// * `setIfNull` 对第一次初始化变量可使用setIfNull函数
/// * `create(namespace)` 通过create函数创建一个独立的存储空间。
/// ### 参看相关类：[CacheLib]
///
class Storage {
  static final GetStorage _box = GetStorage();
  static Future<bool> init() async {
    return await GetStorage.init();
  }

  static Future reset() async => await _box.erase();
  static get(String key) => _box.read(key);
  static setInMemory(String key, value) => _box.writeInMemory(key, value);
  static Future save() async => await _box.save();
  static getKeys() => _box.getKeys();

  static Future setIfNull(key, value) async =>
      await _box.writeIfNull(key, value);
  static Future set(key, value) async => await _box.write(key, value);
  static has(String key) => _box.hasData(key);
  static Future remove(String key) async => await _box.remove(key);

  static Future<GetStorage?> create(String namespace) async {
    GetStorage newStorage = GetStorage(namespace);
    bool done = await GetStorage.init(namespace);
    if (done) return newStorage;
    return null;
  }
}

/// CacheLib 是一个数据的缓存组件，可以通过`setJS`函数与Js Runtime联动.
/// 如果是网络资源可以直接使用get(url)方式，如果是非网络资源，需要先通过set设置
/// * 非网络资源的cache。set指定ttl存留disk的时间(单位为秒)，如果不指定或指定为0则表示
/// 不进行生存期控制（default为null）；指定shouldIcfs为true时数据同时上传到icfs网络，
/// 并在本地查不到时从icfs网络获取（default为false）。在每次get数据时重新计时。例如：
///  ``` dart
///  //设置key，超时5分钟，不需要同步到icfs。5分钟没有调取key的话，自动删除且没有网络备份
///  cache = CacheLib();
///  await cache.set("key":{"a":1,"b":2},ttl:300);
///  //设置key1,超时1个月，不存在时从icfs网络获取
///  await cache.set("key1":"abcd",ttl:60*60*24*30,shouldIcfs:true);
///  //设置key2，长存本地
///  await cache.set("key2":123);
/// ```
///
/// * 网络资源的cache ，接受三种网络资源格式。1、http或https协议，2、icfs协议
/// 3、icns协议是icfs的name机制。可以设定ttl，存留在disk的超时秒数。shouldIcfs对于
/// 网络资源没有作用。isRaw代表返回的网络资源是二进制格式，默认返回json格式。例如：
/// ```dart
/// //60秒http资源
/// var a = cache.get("http://www.pic.com/pic.jpg",ttl=60);
/// //不设定定时器,通过icfs获取数据
/// var b= cache.get("icfs://bafk43jqbedaadoyxmpdk33h4oicrwfqlnqu6dnjldk3et54gm6ygh5p5e223k");
/// //不设定定时器，通过icns获取数据
/// var c=get("icns://bafzm3jqbebjfhmz99drvv7vbx4lpumwzt53qiyb2hnz68y6ymumkjsifjetrw");
/// //不设定定时器，通过icfs获取二进制图片数据
/// var c=get("icfs://bafzm3jqbebjfhmz99drvv7vbx4lpumwzt53qiyb2hnz68y6ymumkj397a6fwa",isRaw:true);
/// ```
/// * 联动Js Runtime
/// ```dart
/// CacheLib cache = CacheLib();
/// JsLib jsLib = JsLib();
/// cache.setJS(jsLib);
/// jsLib.setCache(cache);
/// //执行以下语句后，设置key3为123，同时通过jsLib.getVar("key3")也将返回123
/// await cache.set("key3":123);
/// await JsLib.getVar("key3");
/// //相应的，如果在jsLib设置key，也将在cache.get中获得同样的数据
/// JsLib.setVar("key4",{"v1":"abc","v2":"xyz"},ttl:60*60*24,shouldIcfs:true);
/// await cache.get("key3");
/// ```
/// * Provider通过配置json，也可以联动Js runtime 和Cache
/// 在config中调用js语句,系统将自动通过sendMessage/onMessage联动js runtime进而联动cache
/// ```js
///   //js脚本,设置cache的key4为value4，同步到icfs上，同时设置超时1分钟从disk删除
///   setVar("key4","value4",60,true)
/// ```
/// * 关于ttl的使用：没次set或get都可以设置ttl（秒为单位），规则是：如果设置新的ttl，则使用新的ttl，
/// 如果不设置新ttl或新ttl为0，则使用上一次ttl。如果上一次ttl也为0或空，则不设置定时器，key/value永久保存在disk中
/// * 参看关联类 Storage、ICFS、JsLib
class CacheLib {
  final String baseIcfsUrl =
      Constant.baseIcfsUrl; // "http://120.27.137.222:18042/icfs";
  final String baseIcnsUrl =
      Constant.baseIcnsUrl; //"http://120.27.137.222:18042/ipns";
  final ICFS icfs = ICFS(headers: {});
  Map<String, dynamic> cacheTimer = {};
  JsLib? jsLib;

  CacheLib([this.jsLib]);
  void loadTTL() {
    //载入TTL配置
    List? keys = Storage.getKeys().toList();
    keys
        ?.where((item) => item.toString().startsWith("TTL_"))
        .forEach((item) async {
      Map<String, dynamic> ttlItem = Storage.get(item.toString());
      int resetTTL;
      if (ttlItem["expire"] != null) {
        resetTTL = DateTime.fromMicrosecondsSinceEpoch(ttlItem["expire"])
            .difference(DateTime.now())
            .inSeconds;
      } else {
        resetTTL = 0;
      }
      if (resetTTL == 0) {
        resetTTL = ttlItem["ttl"];
      }
      String hashKey = item.toString().substring(4);
      print("new cache ttl:$hashKey,$resetTTL,$ttlItem");
      await _refreshCacheTimer(
        hashKey,
        ttl: resetTTL,
        shouldIcfs: ttlItem["shouldIcfs"],
      );
    });
  }

  void setJS(jsLib) {
    this.jsLib = jsLib;
  }

  Future<String?> _setCacheIcfs(String key, dynamic value) async {
    try {
      String tempFile = DateTime.now().microsecondsSinceEpoch.toString();
      File file =
          await FileLib.writeAsBytes(tempFile, utf8.encode(json.encode(value)));
      String cid = (await icfs.add(file))["Hash"];
      return cid;
    } catch (e) {
      print("_setCacheIcfs 出现错误:$e");
      return null;
    }
  }

  Future<void> _refreshCacheTimer(String hashKey,
      {int? ttl, required bool shouldIcfs}) async {
    //如果ttl==0 代表不启用定时删除

    ac.Timer? oldTimer = cacheTimer[hashKey]?["timer"];
    int? oldTTL = cacheTimer[hashKey]?["ttl"];
    if (oldTimer != null) {
      print("重新计时:$hashKey");
      oldTimer.cancel();
    }
    //oldTTL!=0  ,ttl==0 使用oldTTL
    //oldTTL!=0  ,ttl!=0 使用ttl
    //oldTTL==0  ,ttl==0 不计时
    //oldTTL==0  ,ttl!=0 使用ttl
    int? newTTL;
    if ((ttl ?? 0) != 0) {
      newTTL = ttl!;
    } else {
      newTTL = oldTTL;
    }
    if ((newTTL ?? 0) != 0) {
      int expire =
          DateTime.now().add(Duration(seconds: newTTL!)).microsecondsSinceEpoch;
      await Storage.set("TTL_$hashKey",
          {"ttl": newTTL, "expire": expire, "shouldIcfs": shouldIcfs});
      ac.Timer timer = ac.Timer(Duration(seconds: newTTL), () async {
        print("从disk中清除cache:$hashKey");
        String? cid;
        if (shouldIcfs) {
          cid = await _setCacheIcfs(hashKey, Storage.get(hashKey));
          print("$hashKey 已经存入icfs，cid=$cid");
          await Storage.set("CID_$hashKey", cid);
        }
        await Storage.remove("TTL_$hashKey");
        await Storage.remove(hashKey);
        cacheTimer[hashKey] = null;
      });
      cacheTimer[hashKey] = {"timer": timer, "ttl": newTTL};
    }
  }

  Future<void> _set(String key, dynamic value,
      {int? ttl, bool shouldIcfs = false}) async {
    await Storage.set(key, value);
    await _refreshCacheTimer(key, ttl: ttl, shouldIcfs: shouldIcfs);
    // if (shouldIcfs) {
    //   _setCacheIcfs(key, value);
    // }
  }

  Future<void> set(String key, dynamic value,
      {int? ttl, bool shouldIcfs = false}) async {
    await jsLib?.setVarFromCache(key, value);
    await _set(key, value, ttl: ttl, shouldIcfs: shouldIcfs);
  }

  Future<void> setFromJs(String key, dynamic value,
      [int? ttl, bool shouldIcfs = false]) async {
    await _set(key, value, ttl: ttl, shouldIcfs: shouldIcfs);
  }

  Future<dynamic> get(String key,
      {int? ttl,
      bool shouldIcfs = false,
      bool icfsBindTTL = true,
      bool isRaw = false}) async {
    var data;
    String? url;
    var keyToLower = key.toLowerCase();
    var hashKey;
    if ("icfs://".matchAsPrefix(keyToLower) != null) {
      url = "$baseIcfsUrl/${keyToLower.substring(7)}";
    } else if ("icns://".matchAsPrefix(keyToLower) != null) {
      url = "$baseIcnsUrl/${keyToLower.substring(7)}";
    } else if ("http://".matchAsPrefix(keyToLower) != null) {
      url = keyToLower;
    } else if ("https://".matchAsPrefix(keyToLower) != null) {
      url = keyToLower;
    }
    //非网络取数
    if (url == null) {
      if (Storage.has(key)) {
        await _refreshCacheTimer(key, ttl: ttl, shouldIcfs: shouldIcfs);
        return Storage.get(key);
      }
      //String? cid = Storage.get("CID_$key");
      String? cid = Storage.get("CID_$key");
      if (cid != null) {
        data = await icfs.cat(cid);
        await Storage.set(key, data);
        _refreshCacheTimer(key, ttl: ttl, shouldIcfs: shouldIcfs);
        return data;
      }
      return null;
    }
    //print(Storage.getKeys().toList());
    debugPrint("fetch from disk....");
    //网络取数，先从cache中取，没有再从网络取
    hashKey = BufferLib.base64ToHexString(
        HashLib.md5(keyToLower)); //url 要考虑替换为keyToLower,已替换
    if (Storage.has(hashKey)) {
      _refreshCacheTimer(hashKey, ttl: ttl, shouldIcfs: shouldIcfs);
      return Storage.get(hashKey);
    }
    debugPrint(hashKey);
    var responseType = isRaw ? ResponseType.bytes : ResponseType.json;
    debugPrint("fetch from network with responseType [$responseType]....");
    data = await HttpLib.getData(url, responseType: responseType);

    await Storage.set(hashKey, data);
    _refreshCacheTimer(hashKey, ttl: ttl, shouldIcfs: shouldIcfs);
    return data;
  }
}
