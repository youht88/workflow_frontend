import 'dart:convert';

import 'package:flutter_js/flutter_js.dart';
import 'package:workflow/util/cache/cache_lib.dart';

class JsLib {
  late JavascriptRuntime jsRuntime;
  CacheLib? cache;
  JsLib([this.cache]) {
    jsRuntime = getJavascriptRuntime(xhr: true);
    registFunction(r"""
        __DATA__={}
        function setVar(k,v,ttl=0,shouldIcfs=false){
          __DATA__[k]=v
          var temp = {}
          temp[k]={"value":v,"ttl":ttl,"shouldIcfs":shouldIcfs}
          sendMessage('updateData',JSON.stringify(temp))
        }
        function getVar(k){
          return __DATA__[k]
        }
        function variable(){
          return JSON.stringify(__DATA__)
        }
        function toString(obj){
          return JSON.stringify(JSON.stringify(obj))
        }
      """);
/*    registFunction(r""" 
function fetch(url, options) {
	options = options || {};
	return new Promise( (resolve, reject) => {
		const request = new XMLHttpRequest();
		const keys = [];
		const all = [];
		const headers = {};

		const response = () => ({
			ok: (request.status/100|0) == 2,		// 200-299
			statusText: request.statusText,
			status: request.status,
			url: request.responseURL,
			text: () => Promise.resolve(request.responseText),
			json: () => { 
				// TODO: review this handle because it may discard \n from json attributes
				try {
					// console.log('RESPONSE TEXT IN FETCH: ' + request.responseText);
					return Promise.resolve(JSON.parse(request.responseText));
				} catch (e) {
					// console.log('ERROR on fetch parsing JSON: ' + e.message);
					return Promise.resolve(request.responseText);
				}
			},
			blob: () => Promise.resolve(new Blob([request.response])),
			clone: response,
			headers: {
				keys: () => keys,
				entries: () => all,
				get: n => headers[n.toLowerCase()],
				has: n => n.toLowerCase() in headers
			}
		});

		request.open(options.method || 'get', url, true);

		request.onload = () => {
			request.getAllResponseHeaders().replace(/^(.*?):[^\S\n]*([\s\S]*?)$/gm, (m, key, value) => {
				keys.push(key = key.toLowerCase());
				all.push([key, value]);
				headers[key] = headers[key] ? `${headers[key]},${value}` : value;
			});
			resolve(response());
		};

		request.onerror = reject;

		request.withCredentials = options.credentials=='include';

		if (options.headers) {
			if (options.headers.constructor.name == 'Object') {
				for (const i in options.headers) {
					request.setRequestHeader(i, options.headers[i]);
				}
			} else { // if it is some Headers pollyfill, the way to iterate is through for of
				for (const header of options.headers) {
					request.setRequestHeader(header[0], header[1]);
				}
			}
		}

		request.send(options.body || null);
	});
}
      """);
*/
    jsRuntime.onMessage("updateData", (args) {
      if (args is Map) {
        args.entries.forEach((item) {
          cache?.setFromJs(item.key, item.value["value"], item.value["ttl"],
              item.value["shouldIcfs"]);
        });
      }
    });
  }
  void setCache(cache) {
    this.cache = cache;
  }

  String eval(String code) {
    //print("!!!!!!!!!!!!!!!!!!!!!!");
    //jsRuntime.dartContext = {"a": 100, "b": () => DateTime.now()};
    //var a=JSStringPointer.array(["abcd","xyz"]);
    //JSContext()
    //print(jsRuntime.dartContext["b"]());
    //JSContext.create();
    //jsRuntime.evaluate(
    //    r"var a=4;this.sendMessage('aaa',JSON.stringify(`abc ok ${a}`))");
    //jsRuntime.evaluate(r"dartContext");
    final test = jsRuntime.evaluate(code).stringResult;
    return test;
  }

  void registFunction(String script) {
    eval(script);
  }

  void callFunction(String fnName, String arg) {}

  Future<void> setVar(String name, dynamic value,
      {int ttl = 60, bool shouldIcfs = false}) async {
    eval("""__DATA__["$name"] = JSON.parse('${json.encode(value)}')""");
    await cache?.setFromJs(name, value, ttl, shouldIcfs);
  }

  Future<void> setVarFromCache(String name, dynamic value) async {
    eval("""__DATA__["$name"] = JSON.parse('${json.encode(value)}')""");
  }

  dynamic getVar(String name) {
    return json.decode(eval("""
             JSON.stringify(__DATA__['$name']??null)
           """));
  }

  void setVarNoCache(String name, dynamic value) {
    eval("""$name=JSON.parse('${json.encode(value)}')""");
  }

  dynamic getVarNoCache(String name) {
    return json.decode(eval("JSON.stringify($name)"));
  }
}
