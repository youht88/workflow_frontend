import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

/// * 简单封装dio的get、post和updateFile。
/// get、post默认返回json类型，如果需要返回的bytes类型（比如图片、声音、食品、pdf文件等）
/// 需要设置 responseType: ResponseType.bytes
/// ```dart
/// Map<String,dynamic> a = await HttpClient.getData("http://www.api.com");
/// Map<String,dynamic> b = awati HttpClient.postData("http://www.post.com",body:{"param1":value1});
/// Uint8List c = await HttpClient.getData("http://www.img.com/img.jpg",responseType:ResponseType.bytes);
/// ```
///
class HttpLib {
  static const utf8Decoder = Utf8Decoder();

  static Future<dynamic> getDataRaw(String url,
      {Map<String, dynamic>? headers,
      Map<String, dynamic>? query,
      Map<String, dynamic>? body}) async {
    return getData(
      url,
      query: query,
      body: body,
      responseType: ResponseType.bytes,
    );
  }

  // stream模式的http请求返回的将是Stream<Uint8List>
  // 调用者应当对返回的stream做相应的处理，必须传入onData,onError,onDone三个函数,例如:
  // onData:(chunk) {
  //   // 在这里处理每次接收到的chunk
  //   debugPrint(chunk);
  // },
  // onError: (error) {
  //   // 错误处理
  //   debugPrint('Error: $error');
  // },
  // onDone: () {
  //   // 当stream接收完毕时的处理
  //   debugPrint('Stream Done');
  // }
  static Future<Stream<Uint8List>> getDataStream(String url,
      {Map<String, dynamic>? headers,
      Map<String, dynamic>? query,
      Map<String, dynamic>? body,
      required Function(String) onData,
      required Function(Error) onError,
      required Function() onDone}) async {
    final responseData = await getData(
      url,
      headers: headers,
      query: query,
      body: body,
      responseType: ResponseType.stream,
    );
    final stream = responseData.stream;
    stream.listen(
        (chunk) {
          final chunkString = utf8.decode(chunk);
          onData(chunkString);
        },
        cancelOnError: true,
        onError: (error) {
          onError(error);
        },
        onDone: () {
          onDone();
        });
    return stream;
  }

  static Future<dynamic> getData(
    String url, {
    ResponseType responseType = ResponseType.json,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? query,
    Map<String, dynamic>? body,
  }) async {
    Response? response;
    dynamic res;
    debugPrint("Get-URL:$url");
    response =
        await Dio(BaseOptions(responseType: responseType, headers: headers))
            .get(url, queryParameters: query, data: body);
    if (response.statusCode.toString().startsWith('2')) {
      res = response.data;
      return res;
    }
    String msg = '网络异常,${response.statusMessage}';
    throw msg;
  }

  //为什么会报405错误？？？
  static Future<dynamic> postDataRaw(String url,
      {Map<String, dynamic>? query,
      Map<String, dynamic>? headers,
      Map<String, dynamic>? body,
      Function(int, int)? onSendProgress,
      Function(int, int)? onReceiveProgress}) async {
    dynamic res = await postData(url,
        query: query,
        headers: headers,
        body: body,
        responseType: ResponseType.bytes,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress);
    return res;
  }

  // stream模式的http请求返回的将是Stream<Uint8List>
  // 调用者应当对返回的stream做相应的处理，必须传入onData,onError,onDone三个函数,例如:
  // onData:(chunk) {
  //   // 在这里处理每次接收到的chunk
  //   debugPrint(chunk);
  // },
  // onError: (error) {
  //   // 错误处理
  //   debugPrint('Error: $error');
  // },
  // onDone: () {
  //   // 当stream接收完毕时的处理
  //   debugPrint('Stream Done');
  // }
  static Future<Stream<Uint8List>> postDataStream(String url,
      {CancelToken? cancelToken,
      Map<String, dynamic>? headers,
      Map<String, dynamic>? query,
      Map<String, dynamic>? body,
      required Function(String) onData,
      required Function(Error) onError,
      required Function() onDone}) async {
    final responseData = await postData(
      url,
      headers: headers,
      query: query,
      body: body,
      responseType: ResponseType.stream,
    );
    final stream = responseData.stream;
    stream.listen(
        (chunk) {
          final chunkString = utf8.decode(chunk);
          onData(chunkString);
        },
        cancelOnError: true,
        onError: (error) {
          onError(error);
        },
        onDone: () {
          onDone();
        });
    return stream;
  }

  static Future postData(String url,
      {CancelToken? cancelToken,
      ResponseType responseType = ResponseType.json,
      Map<String, dynamic>? headers,
      Map<String, dynamic>? query,
      Map<String, dynamic>? body,
      Function(int, int)? onSendProgress,
      Function(int, int)? onReceiveProgress}) async {
    Response? response;
    dynamic res;
    var dio = Dio(BaseOptions(responseType: responseType, headers: headers));
    //dio.interceptors.add(ResponseInterceptor());
    debugPrint("Post-URL:$url");
    response = await dio.post(url,
        data: body,
        cancelToken: cancelToken,
        queryParameters: query,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress);
    if (response.statusCode.toString().startsWith('2')) {
      res = response.data;
      return res;
    }
    String msg = '网络异常[${response.statusCode}],${response.statusMessage}';
    throw msg;
  }

  static uploadFile(String url, File file,
      {Map<String, dynamic>? body,
      ResponseType responseType = ResponseType.json,
      Map<String, dynamic>? headers,
      Function(int, int)? onSendProgress,
      Function(int, int)? onReceiveProgress}) async {
    String path = file.path;
    final res;
    var name = path.substring(path.lastIndexOf("/") + 1, path.length);
    var suffix = name.substring(name.lastIndexOf(".") + 1, name.length);
    FormData formData = FormData.fromMap(
        {"file": await MultipartFile.fromFile(path, filename: name)});

    var response =
        await Dio(BaseOptions(responseType: responseType, headers: headers))
            .post<String>(url,
                data: formData,
                queryParameters: body,
                onSendProgress: onSendProgress,
                onReceiveProgress: onReceiveProgress);
    if (response.statusCode == 200) {
      // form 形式默认返回 response.data是String类型
      res = json.decode(response.data ?? "");
      return res;
    }
    String msg = '网络异常,${response.statusMessage}';
    throw msg;
  }
}
