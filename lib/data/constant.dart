class Constant {
  static String version = '0.1.1';
  static String baseUrl = "http://www.baidu.com/";

  static const String baseIcfsUrl = "http://relates.grjky.cn:5002/icfs";
  // static String get baseIcfsUrl => (){
  //   var lst = ["http://relates.grjky.cn:5002/icfs"];
  //   lst.shuffle();
  //   return lst[0];
  // }();

  //"http://120.27.137.222:18042/icfs";
  //static const String baseIcfsUrl = "http://localhost:8080/icfs";
  static const String baseIcnsUrl = "http://relates.grjky.cn:5002/ipns";
  //"http://120.27.137.222:18042/ipns";
  static const String baseIcfsApiUrl = "http://relates.grjky.cn:5001";
  //"http://120.27.137.222:15042";
  //static const String baseIcfsApiUrl = "http://localhost:5001";

  //static const String endpoint = "http://youht.cc:18042";
  static const String endpoint = "http://youht.cc:18072";
  //dev
  //static const String endpoint = "http://localhost:3000";
  //static const String endpoint = "http://192.168.86.62:3000";
  static const String grpcHost = "localhost";
  static const int grpcPort = 7788;
  //prod
  //static const String endpoint = "http://youht.cc:18072";

  static const String langchainChat = "$endpoint/langchain/chat";
  static const String langchainAgent = "$endpoint/langchain/agent";
  static const String getAction = "$endpoint/langchain/get_action";
  static const String parseAction = "$endpoint/langchain/parse_action";
  static const String langchainFakeStream = "$endpoint/langchain/fake_stream";

  static const String testES = "$endpoint/es/test/test4";

  static const String getUserProfile = "$endpoint/v1/user/get_user_profile";
  static const String setUserProfile = "$endpoint/v1/user/set_user_profile";
  static const String updateUserProfile =
      "$endpoint/v1/user/update_user_profile";

  static const String newConversation = "$endpoint/v1/chat/new_conversation";
  static const String addChat = "$endpoint/v1/chat/add_chat";
  static const String getChats = "$endpoint/v1/chat/get_chats";
  static const String insertConversationTitles =
      "$endpoint/v1/chat/insert_conversation_title";
  static const String getConversationTitles =
      "$endpoint/v1/chat/get_conversation_titles";
  static const String updateConversationTitle =
      "$endpoint/v1/chat/update_conversation_title";
  static const String newContent = "$endpoint/v1/chat/new_content";
  static const String removeConversation =
      "$endpoint/v1/chat/remove_conversation";

  static const String exportProject = "$endpoint/v1/project/export";
  static const String importProject = "$endpoint/v1/project/import";
  static const String removeProject = "$endpoint/v1/project/remove";
}
