import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/chat/system/chat_system_controller.dart';

class ChatSystemWidget extends StatelessWidget {
  late ChatSystemController chatSystemController;
  // content可以是Widget或者String或者List<String>
  // 如果content是Widget则说明是静态的组件
  // 如果content是String则使用textStyle样式创建一个动态的Text文本。该文本可以由chatSystemController.content动态修改
  // 如果content是List<String>则使用textStyle样式创建一个流式WrapFlow的按钮组,按钮的文字为对应的content列表文字。按钮的默认动作是将文字发送动ChatUser，通常是进一步提问
  // 可以动态地由文字切换到按钮组
  // transpanrentBackground在为按钮组忽略，恒定为透明
  ChatSystemWidget(
      {super.key,
      required content,
      transparentBackground,
      TextStyle? textStyle}) {
    assert(content is Widget || content is String || content is List<String>);
    Get.create(() => ChatSystemController());
    chatSystemController = Get.find<ChatSystemController>();
    chatSystemController.transpanrentBackground = transparentBackground;
    if (content is List<String>) {
      chatSystemController.transpanrentBackground = true;
    }
    chatSystemController.widget = this;
    chatSystemController.updateContentWidget(content, textStyle);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Center(
          child: GetBuilder(
              init: chatSystemController,
              global: false,
              builder: (_) {
                return Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(5)),
                      shape: BoxShape.rectangle,
                      color: chatSystemController.transpanrentBackground
                          ? Colors.transparent
                          : Colors.black12,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: chatSystemController.contentWidget,
                    ));
              }),
        ));
  }
}
