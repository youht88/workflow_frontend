import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';
import 'package:workflow/workflow/chat/system/chat_system_widget.dart';
import 'package:workflow/workflow/myprofile.dart';

class ChatSystemController extends GetxController {
  String? content;
  List<String>? contents;
  bool transpanrentBackground = true;
  late Widget contentWidget;
  late ChatSystemWidget widget;
  late ChatController chatController;
  late MyProfile myProfile;

  @override
  onInit() {
    super.onInit();
    chatController = Get.find<ChatController>();
    myProfile = chatController.workflowController.myProfile;
  }

  updateContentWidget(content, [textStyle]) {
    if (content is Widget) {
      contentWidget = content;
    } else if (content is String) {
      this.content = content;
      contentWidget = GetBuilder(
          init: this,
          global: false,
          builder: (_) {
            return SelectableText(this.content!, style: textStyle);
          });
    } else {
      contents = content;
      transpanrentBackground = true;
      contentWidget = GetBuilder(
          init: this,
          global: false,
          builder: (_) {
            return Center(
                child: Wrap(
                    spacing: 4,
                    runSpacing: 4,
                    children: contents!
                        .map((item) => ElevatedButton(
                            onPressed: () async {
                              await chatController.addUserChat(item);
                              await chatController.llmMessage(
                                item,
                                true,
                              );
                            },
                            child: Text(item, style: textStyle)))
                        .toList()));
          });
    }
    update();
  }
}
