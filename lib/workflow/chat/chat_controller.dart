library chat_controller;

import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';
import 'package:workflow/data/constant.dart';
import 'package:workflow/util/cache/cache_lib.dart';
import 'package:workflow/util/network/http_util.dart';
import 'package:workflow/workflow/chat/action_message.dart';
import 'package:workflow/workflow/chat/ai/chat_ai_widget.dart';
import 'package:workflow/workflow/chat/chat_page.dart';
import 'package:workflow/workflow/chat/magic_message.dart';
import 'package:workflow/workflow/chat/system/chat_system_widget.dart';
import 'package:workflow/workflow/chat/user/chat_user_widget.dart';
import 'package:workflow/workflow/event/chat_system_event.dart';
import 'package:workflow/workflow/project/project_controller.dart';
import 'package:workflow/workflow/tools/bottom_confirm_widget.dart';
import 'package:workflow/workflow/tools/bottom_input_widget.dart';
import 'package:workflow/workflow/work/work_controller.dart';
import 'package:workflow/workflow/workflow_controller.dart';

enum MessageRole { system, assistant, user }

extension MessageRolEnumExtension on MessageRole {
  static MessageRole fromString(String value) {
    switch (value) {
      case 'ai':
      case 'assistant':
        return MessageRole.assistant;
      case 'system':
        return MessageRole.system;
      case 'user':
        return MessageRole.user;
      default:
        throw ArgumentError('Invalid value: $value');
    }
  }
}

class Message {
  late MessageRole role;
  late String content;
  Message(this.role, this.content);
  Message.fromJson(json) {
    role = json['role'];
    content = json['content'];
  }
  Map<String, String> toJson() {
    return {"role": role.name, "content": content};
  }
}

class ChatMessage {
  late String id;
  late DateTime createDateTime;
  late Widget widget;
  ChatMessage({String? id, DateTime? createDateTime, required this.widget}) {
    id = id ?? const Uuid().v4();
    createDateTime = createDateTime ?? DateTime.now();
  }
}

class ConversationTitle {
  late String id;
  late DateTime createDateTime;
  late String title;
  ConversationTitle({required this.id, String? create, required this.title}) {
    if (create != null) {
      createDateTime = DateTime.parse(create);
    } else {
      createDateTime = DateTime.now();
    }
  }
}

class ChatController extends GetxController {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  late WorkflowController workflowController;
  bool hasPostInit = false;
  EventBus eventBus = EventBus();
  late ChatWidget widget;
  Map<String, List<ChatMessage>> chatMessagesMap = {};
  String currentConversationId = "";
  List<ChatMessage> get currentConversationChatMessages =>
      chatMessagesMap[currentConversationId] ?? [];
  List<ConversationTitle> conversationTitles = [];
  String currentConversationTitle = "";
  //List<Widget> chats = [];
  ScrollController chatScrollController = ScrollController();
  TextEditingController editController = TextEditingController();
  bool isAccountCopying = false;
  bool isPrivatekeyCopying = false;

  @override
  onInit() async {
    super.onInit();
    Get.put(WorkflowController());
    workflowController = Get.find<WorkflowController>();
    final account = workflowController.myProfile.accountMap['account'];
    //load chat conversation titles
    await getConversationTitles(account);
    //load chat messages from remote
    if (conversationTitles.isNotEmpty) {
      await loadConversation(account, currentConversationId);
    } else {
      await newConversation();
    }
    update();
  }

  getConversationTitles(String account) async {
    List? titles = await HttpLib.getData(Constant.getConversationTitles,
        query: {"account": account});
    debugPrint("conversationTitles:$titles");
    if (titles != null && titles.isNotEmpty) {
      conversationTitles = titles
          .map<ConversationTitle>((item) => ConversationTitle(
              id: item['id'], create: item['create'], title: item['title']))
          .toList();
      update();
      debugPrint("aaa:${workflowController.myProfile.lastConversationId}");
      ConversationTitle? lastConversationTitle =
          conversationTitles.firstWhereOrNull((element) =>
              element.id == workflowController.myProfile.lastConversationId);
      if (lastConversationTitle != null) {
        currentConversationId = lastConversationTitle.id;
      } else {
        currentConversationId = conversationTitles[0].id;
      }
    } else {
      debugPrint("!!!!!!!!!!!!,远程没有发现$account的conversationTitles");
    }
  }

  getChats(String account, String conversationId) async {
    final remoteChats = await HttpLib.getData(Constant.getChats,
        query: {"account": account, "conversationId": currentConversationId});
    //debugPrint("conversationChats:$remoteChats");
    List<ChatMessage> chatMessages = [];
    for (final item in remoteChats) {
      Widget widget;
      switch (item['role']) {
        case 'ai':
          widget = addAiChat(item['content'], saveChat: false);
          break;
        case 'user':
          widget = await addUserChat(item['content'], saveChat: false);
          break;
        case 'system':
          widget = await addSystemChat(item['content'], saveChat: false);
          break;
        default:
          throw ("not valid role!!");
      }
      chatMessages.add(ChatMessage(
          createDateTime: DateTime.parse(item['createDateTime']),
          widget: widget));
    }
    chatMessagesMap[currentConversationId] = chatMessages;
  }

  postInit() {
    if (hasPostInit) return;
    hasPostInit = true;
    eventBus.on<ChatSystemEvent>().listen((event) async {
      if (event.widgetOrText is Widget) {
        await addSystemChat(event.widgetOrText);
      } else {
        await addSystemChat("${event.widgetOrText}");
      }
    });
  }

  // // 发现某个conversation
  // findConversation({required String url}) {
  //   if (url.toLowerCase().startsWith('http://') ||
  //       url.toLowerCase().startsWith('https://')) {
  //     ConversationMap? findedConversationMap =
  //         conversationMapList.firstWhereOrNull(
  //             //有可能第一个条记录是"新开启的话题"的systemWidget
  //             (conversationMap) =>
  //                 conversationMap.messages[0].content == url ||
  //                 conversationMap.messages[1].content == url);
  //     return findedConversationMap;
  //   }
  //   return null;
  // }

  // 获得chatMessages列表中的ChatSystemWidget列表,
  // 默认仅获得动态内容的元素，如果要获得所有元素isDynamic=false
  List<ChatMessage> getSystemChatItems([isDynamic = true]) {
    if (isDynamic) {
      return currentConversationChatMessages
          .where((item) =>
              item.widget is ChatSystemWidget &&
              (item.widget as ChatSystemWidget).chatSystemController.content !=
                  null)
          .toList();
    } else {
      return currentConversationChatMessages
          .where((item) => item.widget is ChatSystemWidget)
          .toList();
    }
  }

  List<ChatMessage> getUserChatItems() {
    return currentConversationChatMessages
        .where((item) => item.widget is ChatUserWidget)
        .toList();
  }

  List<ChatMessage> getAiChatItems() {
    return currentConversationChatMessages
        .where((item) => item.widget is ChatAiWidget)
        .toList();
  }

  Future copyAccount() async {
    isAccountCopying = true;
    update();
    await Clipboard.setData(ClipboardData(
        text: workflowController.myProfile.accountMap['account']));
    await Future.delayed(const Duration(seconds: 2));
    isAccountCopying = false;
    update();
  }

  Future copyPrivateKey() async {
    isPrivatekeyCopying = true;
    update();
    await Clipboard.setData(ClipboardData(
        text: workflowController.myProfile.accountMap['privateKey']));
    await Future.delayed(const Duration(seconds: 2));
    isPrivatekeyCopying = false;
    update();
  }

  Future newConversation() async {
    currentConversationId = const Uuid().v4();
    currentConversationTitle = "";
    await Storage.set("lastConversationId", currentConversationId);
    chatMessagesMap[currentConversationId] = [];
    addSystemChat(
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(child: Container(color: Colors.grey, height: 1)),
          const Text("  新开启的话题  ",
              style: TextStyle(fontSize: 12, color: Colors.grey)),
          Expanded(child: Container(color: Colors.grey, height: 1)),
        ]),
        transparentBackground: true,
        saveChat: false);
    update();
    await HttpLib.getData(Constant.newConversation, query: {
      "account": workflowController.myProfile.accountMap['account'],
      "conversationId": currentConversationId
    });
  }

  Future newContent() async {
    final res = await HttpLib.getData(Constant.newContent, query: {
      "account": workflowController.myProfile.accountMap['account'],
      "conversationId": currentConversationId
    });
    debugPrint(res);
    if (res == null) {
      addSystemChat(
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Expanded(child: Container(color: Colors.grey, height: 1)),
            const Text("  <说些新内容>  ",
                style: TextStyle(fontSize: 12, color: Colors.grey)),
            Expanded(child: Container(color: Colors.grey, height: 1)),
          ]),
          transparentBackground: true,
          saveChat: false);
      update();
    } else {
      addAiChat(res);
    }
  }

  loadConversation(String account, String id) async {
    if (currentConversationId == id &&
        currentConversationChatMessages.isNotEmpty) {
      return;
    }
    currentConversationId = id;
    currentConversationTitle = conversationTitles
        .firstWhere((ConversationTitle item) => item.id == id)
        .title;
    update();
    await HttpLib.postData(Constant.updateUserProfile, query: {
      "account": account,
    }, body: {
      "currentConversationId": currentConversationId
    });
    if (currentConversationChatMessages.isEmpty) {
      await getChats(account, id);
    }
  }

  addEditToChat() async {
    String text = editController.text;
    editController.text = "";
    update();
    if (text.trim() == "") {
      return;
    }
    if (!text.trim().startsWith("set.privateKey") &&
        !text.trim().startsWith("set.mnemonic")) {
      await addUserChat(text);
    }
    debugPrint("为什么出现这里不设断点，后台数据库就会出错的现象?");
    bool isMagicMessage = await magicMessage(text, this);
    if (!isMagicMessage) {
      llmMessage(text, true, 'chat');
      if (workflowController.myProfile.useAction) {
        await actionMessage(text, this);
      }
    }
    update();
  }

  Future editConversationTitle(String id) async {
    final initTitle = conversationTitles
            .firstWhereOrNull((element) => element.id == id)
            ?.title ??
        "";
    final String? newTitle = await Get.bottomSheet(
      SizedBox(
          height: 200,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: BottomInputWidget(
                initText: initTitle,
                titleText: "更改话题的标题",
                infoText: "默认情况下，话题的标题是对话的第一条文字，您可以随时修改",
                onSubmitted: (text) {
                  Get.back(result: text);
                }),
          )),
      backgroundColor: Colors.white,
    );
    debugPrint("!!!!:$newTitle");
    if ((newTitle ?? "") == "") {
      return;
    }
    try {
      await HttpLib.getData(Constant.updateConversationTitle, query: {
        "account": workflowController.myProfile.accountMap["account"],
        "conversationId": id,
        "title": newTitle
      });
    } catch (e) {
      addSystemChat("代码还没有实现", saveChat: false);
    }
    ConversationTitle? finded =
        conversationTitles.firstWhereOrNull((element) => element.id == id);
    finded?.title = newTitle!;
    currentConversationTitle = newTitle!;
    update();
  }

  Future removeConversation(String id) async {
    final bool? isConfirmed = await Get.bottomSheet(
      SizedBox(
          height: 150,
          child:
              BottomConfirmWidget(title: "删除对话", content: "该对话的所有内容将被删除且无法撤回")),
      backgroundColor: Colors.white,
    );
    if (!(isConfirmed ?? false)) {
      return;
    }
    try {
      await HttpLib.getData(Constant.removeConversation, query: {
        "account": workflowController.myProfile.accountMap["account"],
        "conversationId": id
      });
    } catch (e) {
      addSystemChat("代码还没有实现", saveChat: false);
    }
    chatMessagesMap.remove(id);
    conversationTitles.removeWhere((element) => element.id == id);
    if (currentConversationId == id) {
      if (conversationTitles.isNotEmpty) {
        await loadConversation(
            workflowController.myProfile.accountMap["account"],
            conversationTitles.first.id);
      } else {
        await newConversation();
      }
    }
    update();
  }

  loadChatItems(List items) {
    for (final item in items) {
      switch (item["type"]) {
        case 'user':
          _loadUserChatItem(item);
          break;
        case 'ai':
          _loadAiChatItem(item);
          break;
        case 'system':
          _loadSystemChatItem(item);
          break;
        default:
          break;
      }
    }
  }

  _loadUserChatItem(item) async {
    await addUserChat(item["content"]);
  }

  _loadAiChatItem(item) {
    addAiChat(item["content"]);
  }

  _loadSystemChatItem(item) async {
    await addSystemChat(item["content"]);
  }

  Future<ChatUserWidget> addUserChat(String text,
      {bool addToMessages = true, bool saveChat = true}) async {
    ChatUserWidget userWidget = ChatUserWidget(content: text);
    currentConversationChatMessages.insert(0, ChatMessage(widget: userWidget));
    chatScrollController.animateTo(
        chatScrollController.position.maxScrollExtent,
        duration: const Duration(seconds: 3000),
        curve: Curves.easeIn);
    update();
    final account = workflowController.myProfile.accountMap['account'];
    if (saveChat) {
      await HttpLib.postData(Constant.addChat,
          query: {"account": account, "conversationId": currentConversationId},
          body: {"role": "user", "type": "text", "content": text});
    }
    //如果是新话题更新本地及远程conversationTitles,
    bool noTitle = conversationTitles.firstWhereOrNull(
            (element) => element.id == currentConversationId) ==
        null;
    if (noTitle && text != "") {
      currentConversationTitle = text;
      conversationTitles.insert(
          0,
          ConversationTitle(
            id: currentConversationId,
            title: text,
          ));
      await HttpLib.postData(Constant.insertConversationTitles,
          query: {"account": account},
          body: {"conversationId": currentConversationId, "title": text});
    }
    update();
    return userWidget;
  }

  ChatAiWidget addAiChat(dynamic content, {saveChat = true}) {
    ChatAiWidget aiWidget = ChatAiWidget(content: content, saveChat: saveChat);
    currentConversationChatMessages.insert(0, ChatMessage(widget: aiWidget));
    update();
    return aiWidget;
  }

  removeChatItem(Widget item) {
    currentConversationChatMessages
        .removeWhere((element) => element.widget == item);
    update();
  }

  Future<ChatSystemWidget> addSystemChat(dynamic content,
      {bool transparentBackground = false,
      saveChat = true,
      TextStyle? textStyle}) async {
    ChatSystemWidget systemWidget = ChatSystemWidget(
        content: content,
        textStyle: textStyle,
        transparentBackground: transparentBackground);
    currentConversationChatMessages.insert(
        0, ChatMessage(widget: systemWidget));
    update();
    if (saveChat) {
      await HttpLib.postData(Constant.addChat, query: {
        "account": workflowController.myProfile.accountMap['account'],
        "conversationId": currentConversationId
      }, body: {
        "role": "system",
        "type": "text",
        "content": content
      });
    }
    return systemWidget;
  }

  Future<ChatAiWidget?> llmMessage(String text, bool isStream,
      [String mode = 'chat']) async {
    final newMessage = Message(MessageRole.assistant, "");
    ChatAiWidget aiWidget = addAiChat("");
    update();
    try {
      await aiWidget.chatAiController
          .chat(text, message: newMessage, isStream: isStream, mode: mode);
    } catch (e) {
      debugPrint("error: $e");
      removeChatItem(aiWidget);
      addSystemChat(
          const Text("网络错误!", style: TextStyle(color: Colors.orange)));
      return null;
    }
    return aiWidget;
  }

  startUserMagicMessage(
      BuildContext context,
      String text,
      ProjectController projectController,
      WorkController workController) async {
    if (projectController.works.isEmpty) return;
    if (workController.isAiMode || workController.inputElements.isEmpty) {
      debugPrint("AI:${workController.aiFunctionConfig}");
      addAiChat(Text(workController.helloMessage));
      return;
    }
    Map<String, Widget> workPage = workController.generateWorkPage(context);
    String? outString;

    if (GetPlatform.isMobile) {
      outString = await Get.bottomSheet(
        backgroundColor: Colors.white,
        workPage['inputPage']!,
      );
    } else {
      outString = await Get.defaultDialog(
        title: "",
        content: workPage['inputPage']!,
      );
    }
    debugPrint(outString);
    if ((outString ?? "") != "") {
      if (projectController.works[0].workController.outputElements.isNotEmpty) {
        addAiChat(workPage['outputPage']!);
      } else {
        addAiChat(Text(outString!));
      }
    }
  }
}
