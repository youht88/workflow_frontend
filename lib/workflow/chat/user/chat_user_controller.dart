import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';
import 'package:workflow/workflow/chat/user/chat_user_widget.dart';
import 'package:workflow/workflow/myprofile.dart';

class ChatUserController extends GetxController {
  String? content;
  late ChatUserWidget widget;
  late ChatController chatController;
  late MyProfile myProfile;
  late Widget contentWidget;
  @override
  onInit() {
    super.onInit();
    chatController = Get.find<ChatController>();
    myProfile = chatController.workflowController.myProfile;
  }

  updateText(String text) {
    content = text;
    update();
  }

  updateContentWidget(content, [textStyle]) {
    if (content is Widget) {
      contentWidget = content;
    } else {
      this.content = content;
      contentWidget = GetBuilder(
          init: this,
          global: false,
          builder: (context) {
            return SelectableText(this.content!, style: textStyle);
          });
    }
    update();
  }
}
