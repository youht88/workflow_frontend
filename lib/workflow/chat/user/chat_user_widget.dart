import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/chat/user/chat_user_controller.dart';

class ChatUserWidget extends StatelessWidget {
  late ChatUserController chatUserController;
  // content可以是Widget或者String
  // 如果content是Widget则说明是静态的组件
  // 如果content是String则使用textStyle样式创建一个动态的Text文本。该文本可以由chatUserController.content动态修改
  ChatUserWidget({super.key, required content, TextStyle? textStyle}) {
    assert(content is Widget || content is String);
    Get.create(() => ChatUserController());
    chatUserController = Get.find<ChatUserController>();
    chatUserController.widget = this;
    chatUserController.updateContentWidget(content, textStyle);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    shape: BoxShape.rectangle,
                    color: Colors.blue.shade50),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: GetBuilder(
                      init: chatUserController,
                      global: false,
                      builder: (_) => chatUserController.contentWidget),
                )),
          ),
        ),
        const SizedBox(width: 8),
        Container(
          height: 40,
          width: 40,
          decoration:
              const BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
          child: const Icon(Icons.tag_faces, color: Colors.white70),
        )
      ]),
    );
  }
}
