import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:workflow/util/data_util/datetime_lib.dart';
import 'package:workflow/util/data_util/string_lib.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';
import 'package:workflow/workflow/tools/input_widget.dart';

class ChatDrawer extends StatelessWidget {
  ChatController chatController = Get.find<ChatController>();
  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: 250,
      elevation: 5,
      child: ListView(
        children: [
          GetBuilder(
              init: chatController,
              builder: (context) {
                return UserAccountsDrawerHeader(
                  currentAccountPictureSize: const Size.square(72),
                  accountName:
                      Text(chatController.workflowController.myProfile.name),
                  accountEmail: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(children: [
                        const Text("   ID:  "),
                        Text(
                            StringLib.mask(
                                chatController.workflowController.myProfile
                                    .accountMap['account'],
                                maskChar: '*',
                                maskLen: 8),
                            style: const TextStyle(
                                fontSize: 12, color: Colors.black45)),
                        const SizedBox(width: 8),
                        InkWell(
                            child: Icon(
                                chatController.isAccountCopying
                                    ? Icons.done_all
                                    : Icons.copy,
                                color: Colors.white70,
                                size: 14),
                            onTap: () async {
                              chatController.copyAccount();
                            })
                      ]),
                      Row(
                        children: [
                          const Text("密钥:  "),
                          Text(
                              StringLib.mask(
                                  chatController.workflowController.myProfile
                                      .accountMap['privateKey'],
                                  maskChar: '*',
                                  maskLen: 12),
                              style: const TextStyle(
                                  fontSize: 12, color: Colors.black54)),
                          const SizedBox(width: 8),
                          InkWell(
                              child: Icon(
                                  chatController.isPrivatekeyCopying
                                      ? Icons.done_all
                                      : Icons.copy,
                                  color: Colors.white70,
                                  size: 14),
                              onTap: () async {
                                chatController.copyPrivateKey();
                              })
                        ],
                      ),
                    ],
                  ),
                  currentAccountPicture: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(
                          chatController.workflowController.myProfile.avatar),
                    ),
                  ),
                );
              }),
          if (chatController.workflowController.myProfile.designMode)
            ListTile(
              title: const Row(
                children: [
                  Icon(Icons.arrow_back_ios),
                  Text('设计模式'),
                ],
              ),
              onTap: () {
                Get.back();
                Get.back();
              },
            ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InputWidget(
                titleText: "系统性提示:",
                infoText: "`系统性提示`指对话的总体要求。例如“全部用中文回复”、“使用风趣的风格回复”等等",
                initText:
                    chatController.workflowController.myProfile.systemMessage,
                onSubmitted: (text) async {
                  await chatController.workflowController.myProfile
                      .updateUserProfile(
                          chatController.workflowController.myProfile
                              .accountMap['account'],
                          {"systemMessage": text});
                  chatController.workflowController.myProfile.systemMessage =
                      text;
                }),
          ),
          const Stack(
            children: [
              Divider(),
              //Align(alignment: Alignment.center, child: Text("话题列表"))
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: ElevatedButton.icon(
                onPressed: () {
                  chatController.scaffoldKey.currentState!.openEndDrawer();
                  chatController.newConversation();
                },
                icon: const Icon(Icons.new_releases),
                label: const Text("新话题")),
          ),
          SizedBox(
            //Expanded( 如果替换成Expanded就会出错
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: chatController.conversationTitles
                      .map((item) => GestureDetector(
                          onTap: () async {
                            chatController.scaffoldKey.currentState!
                                .openEndDrawer();
                            await chatController.loadConversation(
                                chatController.workflowController.myProfile
                                    .accountMap['account'],
                                item.id);
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 2.0),
                            child: Slidable(
                              startActionPane: ActionPane(
                                  extentRatio: 0.5,
                                  motion: const ScrollMotion(),
                                  children: [
                                    SlidableAction(
                                        backgroundColor: Colors.white,
                                        foregroundColor: Colors.purple.shade800,
                                        borderRadius: BorderRadius.circular(20),
                                        onPressed: (_) async {
                                          await chatController
                                              .editConversationTitle(item.id);
                                          chatController
                                              .scaffoldKey.currentState!
                                              .openEndDrawer();
                                        },
                                        icon: Icons.text_fields,
                                        label: "编辑标题"),
                                  ]),
                              endActionPane: ActionPane(
                                  extentRatio: 0.4,
                                  motion: const ScrollMotion(),
                                  children: [
                                    SlidableAction(
                                        backgroundColor: Colors.white,
                                        foregroundColor: Colors.red.shade800,
                                        borderRadius: BorderRadius.circular(25),
                                        onPressed: (_) async {
                                          await chatController
                                              .removeConversation(item.id);
                                          chatController
                                              .scaffoldKey.currentState!
                                              .openEndDrawer();
                                        },
                                        icon: Icons.delete,
                                        label: "删除")
                                  ]),
                              child: SizedBox(
                                width: double.infinity,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 8),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(item.title,
                                          overflow: TextOverflow.ellipsis),
                                      const SizedBox(height: 4),
                                      Text(
                                          "${DateTimeLib.format(item.createDateTime, "YYYY-MM-DD")} ${DateTimeLib.format(item.createDateTime, "HH:mm")}",
                                          style: const TextStyle(
                                              fontSize: 8,
                                              color: Colors.black38))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )))
                      .toList()),
            ),
          )
        ],
      ),
    );
  }
}
