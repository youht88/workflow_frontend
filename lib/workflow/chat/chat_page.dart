import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';
import 'package:workflow/workflow/chat/chat_drawer_page.dart';

class ChatWidget extends StatelessWidget {
  late ChatController controller;

  ChatWidget({super.key}) {
    Get.put(ChatController(), permanent: true);
    controller = Get.find<ChatController>();
    controller.widget = this;
    controller.postInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: controller.scaffoldKey,
        appBar: AppBar(
            leading: InkWell(
                onTap: () {
                  controller.scaffoldKey.currentState!.openDrawer();
                },
                child: const Icon(Icons.list_alt)),
            title: GetBuilder(
                init: controller,
                global: false,
                builder: (context) {
                  return SizedBox(
                    width: 300,
                    child: Center(
                      child: Text(controller.currentConversationTitle,
                          overflow: TextOverflow.fade, maxLines: 1),
                    ),
                  );
                })),
        drawer: ChatDrawer(),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Expanded(
                  child: GetBuilder(
                      init: controller,
                      builder: (_) {
                        return ListView.builder(
                            controller: controller.chatScrollController,
                            reverse: true,
                            //itemCount: controller.chatMessages.length,
                            //itemBuilder: (context, idx) =>
                            //  return controller.chatMessages[idx].widget
                            itemCount: controller
                                .currentConversationChatMessages.length,
                            itemBuilder: (context, idx) => controller
                                .currentConversationChatMessages[idx].widget);
                      })),
              const SizedBox(height: 8),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                        controller: controller.editController,
                        decoration: InputDecoration(
                            icon: InkWell(
                              onTap: () async {
                                await controller.newContent();
                              },
                              splashColor: Colors.amberAccent,
                              child: const Icon(Icons.comment_bank,
                                  color: Colors.deepOrange),
                            ),
                            border: const OutlineInputBorder(),
                            //isDense: true,
                            contentPadding: const EdgeInsets.all(10)),
                        minLines: 1,
                        maxLines: 4,
                        style: const TextStyle(fontSize: 14)),
                  ),
                  GetBuilder(
                      init: controller,
                      builder: (context) {
                        return IconButton(
                            onPressed: () async {
                              await controller.addEditToChat();
                              controller.update();
                            },
                            icon: const Icon(Icons.send, color: Colors.blue));
                      })
                ],
              )
            ],
          ),
        ));
  }
}
