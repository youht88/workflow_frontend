import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/data/constant.dart';
import 'package:workflow/util/cache/cache_lib.dart';
import 'package:workflow/util/crypto_util/crypto_lib.dart';
import 'package:workflow/util/crypto_util/hash_lib.dart';
import 'package:workflow/util/network/http_util.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';
import 'package:workflow/workflow/chat/user/chat_user_widget.dart';
import 'package:workflow/workflow/client/client.dart';

Future<bool> magicMessage(String text, ChatController chatController) async {
  final myProfile = chatController.workflowController.myProfile;
  //首先遍历匹配project中的所有work的magicMessage
  final workss = chatController.workflowController.getProjectWorkWidgets();
  for (final works in workss) {
    for (final work in works) {
      debugPrint("magicMessage:${work.workController.magicMessage}");
      if (work.workController.magicMessage != "" &&
          RegExp(work.workController.magicMessage).hasMatch(text)) {
        await chatController.startUserMagicMessage(Get.context!, text,
            work.workController.projectController, work.workController);
        return true;
      }
    }
  }
  //测试grpc
  if (text == "grpc") {
    final rpcClient = RpcClient();
    rpcClient.init();
    await rpcClient.getInfo();
    await rpcClient.close();
  }
  //其次检测系统内置的magicMessage
  if (text.startsWith("/")) {
    final express = text.substring(1);
    switch (express) {
      case 'version':
        chatController.addAiChat("当前版本是:v${Constant.version}");
        return true;
      default:
        return false;
    }
  }
  if (text.toLowerCase().startsWith("http://") ||
      text.toLowerCase().startsWith("https://")) {
    // //查看是否已经有这个conversation
    // ConversationMap? findedConversationMap =
    //     chatController.findConversation(url: text);
    // chatController.removeChatItem(
    //     chatController.currentConversationChatMessages.last.widget);
    // if (findedConversationMap == null) {
    //   chatController.newConversation();
    //   await chatController.addUserChat(text);
    //   chatController.addAiChat("我已新建了这个$text的库,有什么可以帮助你的吗?");
    // } else {
    //   chatController.loadConversation(findedConversationMap.id);
    //   await chatController.addUserChat(text);
    //   chatController.addAiChat("我已导入了这个$text的库,有什么可以帮助你的吗?");
    // }
    return true;
  }
  if (text == "es") {
    final response = await HttpLib.getData(Constant.testES);
    chatController.addSystemChat(json.encode(response));
    return true;
  }
  if (text == "chat") {
    // final response = await HttpLib.getData(Constant.newConversation, query: {
    //   "account": myProfile.accountMap["account"],
    //   "conversationId": chatController.currentConversationMap.id
    // });
    // final response = await HttpLib.postData(Constant.addChat, query: {
    //   "account": myProfile.accountMap["account"],
    //   "conversationId": chatController.currentConversationMap.id
    // }, body: {
    //   "role": "ai",
    //   "type": "text",
    //   "content": "有什么可以帮助你的？",
    // });
    final response = await HttpLib.getData(Constant.getChats, query: {
      "account": myProfile.accountMap["account"],
      "conversationId": chatController.currentConversationId
    });
    // final response = await HttpLib.getData(Constant.getConversationTitles,
    //     query: {"account": myProfile.accountMap["account"]});
    chatController.addSystemChat(json.encode(response));
    //chatController.loadChatItems(response);
    return true;
  }
  if (text == "start") {
    final widget =
        chatController.getUserChatItems().last.widget as ChatUserWidget;
    widget.chatUserController
        .updateText("${widget.chatUserController.content}\n start");
    return true;
  }
  if (text == "close") {
    Get.back();
    return true;
  }
  if (text == "system") {
    // chatController.projectController.eventBus.fire(ChatSystemEvent(
    //     chatController
    //         .projectController.works[0].workController.aiFunctionConfig));
    return true;
  }
  if (text == "debug") {
    //chatController.projectController.eventBus
    //    .fire(ChatSystemEvent(chatController.currentConversationId));
    await chatController.addSystemChat("debug");
    debugPrint("debug: ${chatController.currentConversationId}");
    return true;
  }
  if (text == 'button') {
    chatController.addSystemChat(["hello", "我是张三", "3+2=?", "如何看待中美关系"]);
    final last = await chatController.addUserChat("hello");
    var counter = 10;
    Timer.periodic(const Duration(seconds: 2), (timer) {
      debugPrint("${timer.tick}");
      if (counter == 0) {
        debugPrint('Cancel timer');
        timer.cancel();
      } else {
        last.chatUserController.updateText("${timer.tick}");
      }
      counter--;
    });
    //await Future.delayed(const Duration(seconds: 10));
    debugPrint("time periodic");
    return true;
  }
  if (text == 'demo') {
    String content =
        "你好，我是AI助手<<CLEAR>>我正在思考你的问题...\n\nSTEP1:你会什么\n\nSTEP2:你将有什么<<CLEAR>>现在我知道答案了<<CLEAR>>答案是:你很牛!";
    String total = "";
    final chatAiWidget = chatController.addAiChat("");
    final query = {"text": content, "lowDelay": 50, "highDelay": 500};
    await HttpLib.getDataStream(Constant.langchainFakeStream, query: query,
        onData: (chunk) {
      debugPrint(chunk);
      if (chunk == "<<CLEAR>>") {
        total = "";
      } else {
        total = total + chunk;
      }
      chatAiWidget.chatAiController.updateText(total);
    }, onError: (error) {
      debugPrint('Error: $error');
      chatController.addSystemChat("出错了！！！");
    }, onDone: () {
      debugPrint('Stream Done');
      chatController.addSystemChat("调用已完成");
      chatController.update();
    });
    return true;
  }
  if (text.startsWith("test:")) {
    await chatController.llmMessage(text.substring(5), true, 'test');
    return true;
  }
  if (text.startsWith("agent:")) {
    await chatController.llmMessage(text.substring(6), true, 'agent');
    return true;
  }
  if (text.startsWith("set.")) {
    String expression = text.substring(4);
    List<String> parts = expression.split('=');
    if (parts.length != 2) return false;
    String account = myProfile.accountMap['account'];
    switch (parts[0]) {
      case 'designMode':
        await chatController.workflowController.myProfile.updateUserProfile(
            account, {"designMode": parts[1].toLowerCase() == 'true'});
        chatController.workflowController.myProfile.designMode =
            Storage.get('designMode');
        chatController.update();
        return true;
      case 'name':
        await chatController.workflowController.myProfile
            .updateUserProfile(account, {"name": parts[1]});
        chatController.workflowController.myProfile.name = Storage.get('name');
        chatController.update();
        return true;
      case 'email':
        await chatController.workflowController.myProfile
            .updateUserProfile(account, {"email": parts[1]});
        chatController.workflowController.myProfile.email =
            Storage.get('email');
        chatController.update();
        return true;
      case 'avatar':
        await chatController.workflowController.myProfile
            .updateUserProfile(account, {"avatar": parts[1]});
        chatController.workflowController.myProfile.avatar =
            Storage.get('avatar');
        chatController.update();
        return true;
      case 'endpoint':
        await chatController.workflowController.myProfile
            .updateUserProfile(account, {"endpoint": parts[1]});
        chatController.workflowController.myProfile.endpoint =
            Storage.get('endpoint');
        chatController.update();
        return true;
      case 'mnemonic':
        final privateKey = HashLib.sha256(parts[1].trim());
        Map<String, dynamic> newAccountMap;
        try {
          newAccountMap = CryptoLib.loadSM2KeyPair(privateKey);
        } catch (e) {
          Get.snackbar("错误", "私钥必须由32位16进制字符组成");
          return true;
        }
        await myProfile.setUserProfile(newAccountMap);
        account = myProfile.accountMap['account'];
        chatController.conversationTitles = [];
        chatController.currentConversationTitle = "";
        chatController.currentConversationId = "";
        await chatController.getConversationTitles(account);
        //load chat messages from remote
        if (chatController.conversationTitles.isNotEmpty) {
          await chatController.loadConversation(
              account, chatController.currentConversationId);
        } else {
          await chatController.newConversation();
        }
        chatController.update();
        return true;
      case 'privateKey':
        Map<String, dynamic> newAccountMap;
        try {
          newAccountMap = CryptoLib.loadSM2KeyPair(parts[1]);
        } catch (e) {
          Get.snackbar("错误", "私钥必须由32位16进制字符组成");
          return true;
        }
        await myProfile.setUserProfile(newAccountMap);
        account = myProfile.accountMap['account'];
        chatController.chatMessagesMap = {};
        chatController.conversationTitles = [];
        chatController.currentConversationTitle = "";
        chatController.currentConversationId = "";
        chatController.update();
        await chatController.getConversationTitles(account);
        //load chat messages from remote
        if (chatController.conversationTitles.isNotEmpty) {
          await chatController.loadConversation(
              account, chatController.currentConversationId);
        } else {
          await chatController.newConversation();
        }
        chatController.update();
        return true;
      case 'useAction':
        await chatController.workflowController.myProfile.updateUserProfile(
            account, {"useAction": parts[1].toLowerCase() == 'true'});
        chatController.workflowController.myProfile.useAction =
            Storage.get('useAction');
        chatController.update();
        return true;
      default:
        return false;
    }
  }
  if (text.startsWith("get.")) {
    String expression = text.substring(4);
    switch (expression) {
      case 'account':
        final accountMap =
            chatController.workflowController.myProfile.accountMap;
        chatController.addAiChat(Text("$accountMap"));
        return true;
      case 'name':
        final name = chatController.workflowController.myProfile.name;
        chatController.addAiChat(Text(name));
        return true;
      case 'designMode':
        final designMode =
            chatController.workflowController.myProfile.designMode;
        final prompt = "把以下语句用中文换一种说法:\n\ndesignMode==$designMode";
        chatController.llmMessage(prompt, true);
        //chatController.addAiChat(Text("$designMode"));
        return true;
      case 'endpoint':
        final endpoint = chatController.workflowController.myProfile.endpoint;
        chatController.addAiChat(Text(endpoint));
        return true;
      default:
        return false;
    }
  }
  return false;
}
