import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_prism/flutter_prism.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:markdown_viewer/markdown_viewer.dart';
import 'package:workflow/data/constant.dart';
import 'package:workflow/util/network/http_util.dart';
import 'package:workflow/workflow/chat/ai/chat_ai_widget.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';
import 'package:workflow/workflow/myprofile.dart';

class ChatAiController extends GetxController {
  String? content;
  late ChatAiWidget widget;
  late Widget contentWidget;
  late ChatController chatController;
  bool isSending = false;
  CancelToken? cancelToken;
  late MyProfile myProfile;
  bool saveChat = false;
  @override
  onInit() {
    super.onInit();
    chatController = Get.find<ChatController>();
    myProfile = chatController.workflowController.myProfile;
  }

  updateText(String text) {
    content = text;
    update();
  }

  updateContentWidget(content, saveChat) {
    if (content is Widget) {
      contentWidget = content;
    } else {
      this.content = content;
      this.saveChat = saveChat;
      if (content != "" && saveChat) {
        HttpLib.postData(Constant.addChat, query: {
          "account": myProfile.accountMap['account'],
          "conversationId": chatController.currentConversationId
        }, body: {
          "role": "ai",
          "type": "text",
          "createDateTime": DateTime.now().toString(),
          "content": content!
        });
      }
      final stopWidget = GestureDetector(
        onTap: () {
          stopSending();
        },
        child: const Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [Icon(Icons.stop, color: Colors.red), Text("取消")]),
        ),
      );
      contentWidget = GetBuilder(
          init: this,
          global: false,
          builder: (_) {
            return this.content == ""
                ? Column(
                    children: [
                      SizedBox(
                        width: 40,
                        child: SpinKitRotatingCircle(
                          color: Colors.purple.shade100,
                          size: 40.0,
                        ),
                      ),
                      GetBuilder(
                          init: this,
                          global: false,
                          builder: (_) {
                            return isSending ? stopWidget : const SizedBox();
                          })
                    ],
                  )
                // : MarkdownBody(
                //         data: newMessage.content,
                //         selectable: true,
                //         onTapLink: (text, href, title) {
                //           debugPrint("$text@$href@$title");
                //           answerRelateQuestion(text);
                //         },
                //         syntaxHighlighter: MySyntaxHighlighter()
                //         //syntaxHighlighter: getHighlighter(),
                //         )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        GetBuilder(
                            init: this,
                            global: false,
                            builder: (_) {
                              return MarkdownViewer(
                                this.content ?? "",
                                styleSheet: const MarkdownStyle(
                                    textStyle: TextStyle(fontSize: 14)),
                                selectable: true,
                                onTapLink: (text, href) async {
                                  debugPrint("$text@$href");
                                  await answerRelateQuestion();
                                },
                                highlightBuilder: (text, language, infoString) {
                                  final prism = Prism(
                                    mouseCursor: SystemMouseCursors.text,
                                  );
                                  return prism.render(
                                      text,
                                      ((language == 'rc') ? null : language) ??
                                          'plain');
                                },
                              );
                            }),
                        GetBuilder(
                            init: this,
                            global: false,
                            builder: (_) {
                              return isSending ? stopWidget : const SizedBox();
                            })
                      ]);
          });
    }
  }

  stopSending() {
    cancelToken?.cancel();
    isSending = false;
    update();
  }

  startSending() {
    cancelToken = CancelToken();
    isSending = true;
    update();
  }

  Future<void> clipborad() async {
    await Clipboard.setData(ClipboardData(text: "$content"));
    Get.snackbar("提示", "已经拷贝到粘贴板");
  }

  Future<void> genSummary() async {
    chatController.addUserChat("做一下总结", addToMessages: false);
    String prompt = "请将以下文字做一个总结";
    chatController.llmMessage("$prompt\n\n$content", true);
  }

  Future<void> genRelateQuestion() async {
    chatController.addUserChat("提出一些相关问题", addToMessages: false);
    //String prompt = "用markdonw表示每个中文问题链接。请针对以下文字用中文启发式联想两到四个相关问题。";
    //String prompt = "用markdonw表示每个中文问题链接。这些中文链接是对以下文字的相关问题。";
    String prompt = "根据以下文字提出2-4个相关的中文问题。要求每个问题必须用markdown链接表示以便用户点击";
    // String prompt =
    //     "Please generate two to four related questions in Chinese using heuristic association for the following text, and represent each Chinese question as a markdown link.";
    chatController.llmMessage("$prompt\n\n$content", true);
  }

  Future<void> answerRelateQuestion() async {
    await chatController.addUserChat("$content");
    // String prompt = "请针对以下文字用中文启发式联想四个相关问题并用markdonw表示每个中文问题链接";
    // // String prompt =
    // //     "Please generate two to four related questions in Chinese using heuristic association for the following text, and represent each Chinese question as a markdown link.";
    chatController.llmMessage("$content", true);
  }

  Future chat(
    String prompt, {
    required Message message,
    String mode = 'chat',
    bool isStream = true,
  }) async {
    List<Map<String, String>> conversationMapList;
    final account = myProfile.accountMap['account'];
    final conversationId = chatController.currentConversationId;
    final systemMessage = myProfile.systemMessage;

    conversationMapList = [
      {"role": "system", "content": systemMessage},
      {"role": "user", "content": prompt}
    ];

    startSending();
    String recievedChunk = "";
    try {
      String langchainUrl;
      if (mode == "chat") {
        langchainUrl = Constant.langchainChat;
      } else {
        langchainUrl = Constant.langchainAgent;
      }

      if (!isStream) {
        final output = await HttpLib.postData(langchainUrl,
            cancelToken: cancelToken,
            body: {
              "messages": conversationMapList,
              "account": account,
              "conversationId": conversationId
            },
            responseType: ResponseType.plain);
        message.content = output;
        content = output;
        stopSending();
      } else {
        final stream = await HttpLib.postDataStream(langchainUrl,
            cancelToken: cancelToken,
            body: {
              "messages": conversationMapList,
              "stream": true,
              "account": account,
              "conversationId": conversationId,
              //"functions":
              //    projectController.works[0].workController.aiFunctionConfig,
            }, onData: (chunk) {
          if ((cancelToken != null) && ((cancelToken?.isCancelled) ?? false)) {
            debugPrint("okokokok");
            stopSending();
            chatController.removeChatItem(widget);
            return;
          }
          // 在这里处理每次接收到的chunk
          recievedChunk = recievedChunk + chunk;
          debugPrint(recievedChunk);
          updateText(recievedChunk);
        }, onError: (error) {
          // 错误处理
          debugPrint('Error: $error');
          stopSending();
        }, onDone: () async {
          // 当stream接收完毕时的处理
          debugPrint('Stream Done');
          if ((cancelToken != null) && ((cancelToken?.isCancelled) ?? false)) {
            debugPrint("如何cancelToken");
          } else {
            debugPrint("正常结束");
            message.content = content!;
            if (message.content == "") {
              debugPrint("error????");
              chatController.removeChatItem(widget);
              return;
            }
            chatController.update();
            if (saveChat) {
              await HttpLib.postData(Constant.addChat, query: {
                "account": myProfile.accountMap['account'],
                "conversationId": chatController.currentConversationId
              }, body: {
                "role": "ai",
                "type": "text",
                "createDateTime": DateTime.now().toString(),
                "content": content!
              });
            }
          }
          stopSending();
          debugPrint("$cancelToken,$isSending");
        });
      }
    } catch (e) {
      debugPrint("error!:$e");
      stopSending();
      rethrow;
    }
  }
}
