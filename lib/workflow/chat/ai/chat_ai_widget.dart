import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:star_menu/star_menu.dart';
import 'package:workflow/workflow/chat/ai/chat_ai_controller.dart';
import 'package:workflow/workflow/menu/menu_params.dart';

class ChatAiWidget extends StatelessWidget {
  late ChatAiController chatAiController;
  // content可以是Widget或者String
  // 如果content是Widget则说明是静态的组件
  // 如果content是String则使用textStyle样式创建一个动态的Text文本。该文本可以由chatSystemController.content动态修改
  ChatAiWidget({super.key, required content, isLoad = false, saveChat}) {
    assert(content is Widget || content is String);
    Get.create(() => ChatAiController());
    chatAiController = Get.find<ChatAiController>();
    chatAiController.widget = this;
    chatAiController.updateContentWidget(content, saveChat);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          Container(
            height: 40,
            width: 40,
            decoration: const BoxDecoration(
                shape: BoxShape.circle, color: Colors.purple),
            child: const Icon(Icons.school, color: Colors.white70),
          ),
          const SizedBox(width: 8),
          Expanded(
            child: Container(
              //color: Colors.white,
              child: SingleChildScrollView(
                  //scrollDirection: Axis.horizontal,
                  child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5)),
                        shape: BoxShape.rectangle,
                        color: Colors.purple.shade50,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: GetBuilder(
                            init: chatAiController,
                            global: false,
                            builder: (_) {
                              return GestureDetector(
                                  onDoubleTap: () {
                                    StarMenuOverlay.displayStarMenu(
                                        context,
                                        StarMenu(
                                            parentContext: context,
                                            onItemTapped:
                                                (idx, menuController) {
                                              menuController.closeMenu!();
                                            },
                                            params: getMenuParams(Get.context!,
                                                useLongPress: true,
                                                centerOffset:
                                                    const Offset(50, -50)),
                                            items: [
                                              InkWell(
                                                  child: const Text("拷贝"),
                                                  onTap: () {
                                                    chatAiController
                                                        .clipborad();
                                                  }),
                                              InkWell(
                                                  child: const Text("总结"),
                                                  onTap: () {
                                                    chatAiController
                                                        .genSummary();
                                                  }),
                                              InkWell(
                                                  child: const Text("相关问题"),
                                                  onTap: () {
                                                    chatAiController
                                                        .genRelateQuestion();
                                                  }),
                                            ]));
                                  },
                                  child: chatAiController.contentWidget);
                            }),
                      )),
                ),
              )),
            ),
          ),
        ]),
      ),
    );
  }
}
