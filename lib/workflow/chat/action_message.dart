import 'package:flutter/material.dart';
import 'package:workflow/data/constant.dart';
import 'package:workflow/util/network/http_util.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';
import 'package:workflow/workflow/event/chat_system_event.dart';

Future<bool> actionMessage(String text, ChatController chatController) async {
  String account =
      chatController.workflowController.myProfile.accountMap['account'];
  String conversationId = chatController.currentConversationId;
  final query1 = {"prompt": text, "mode": "useFewShot"};
  Map<String, dynamic> actionMap =
      await HttpLib.getData(Constant.getAction, query: query1);
  if (["record", "query", "plan"].contains(actionMap["mode"]) &&
      ["diet", "sport", "sign", "behavior", "feel"]
          .contains(actionMap["type"])) {
    final query2 = {
      "action": actionMap['type'],
      "mode": "useFewShot",
      "prompt": text,
      "account": account,
      "conversationId": conversationId
    };
    final parseActionMap =
        await HttpLib.getData(Constant.parseAction, query: query2);
    debugPrint("parseActionMap:$parseActionMap");
    if ((parseActionMap ?? []).isNotEmpty) {
      chatController.addSystemChat("$parseActionMap");
      return true;
    }
  }
  return false;
}
