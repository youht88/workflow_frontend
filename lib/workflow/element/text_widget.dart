import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/tools/input_widget.dart';
import 'package:workflow/workflow/tools/toggle_widget.dart';

import 'element_widget.dart';

class TextWidgetParams {
  Widget child;
  late Map<String, dynamic> widgetJsonData;
  double? width;
  double? height;
  String? text = "Text";
  num? fontSize = 14;
  String? color = "#000000";
  TextWidgetParams(
      {this.child = const SizedBox(), widgetJsonData, this.text = "Text"}) {
    // String jsonData = r'''
    // {
    //   "type": "container",
    //   "args":{
    //     "width":"${width}",
    //     "height":"${height}",
    //     "padding":"${padding}",
    //     "alignment":"${alignment}"
    //   },
    //   "child": {
    //     "type": "text",
    //     "args": {"text": "${text}","style":{"fontSize":"${fontSize}","color":"${color}"}}
    //   }
    // }''';
    Map<String, dynamic> jsonData = {
      "type": "text",
      "args": {
        "text": r"${text}",
        "style": {"fontSize": r"${fontSize}", "color": r"${color}"}
      }
    };

    // Map<String, dynamic> jsonData = {
    //   "type": "expanded",
    //   "child": {
    //     "type": "column",
    //     "children": [
    //       {
    //         "type": "expanded",
    //         "child": {
    //           "type": "text",
    //           "args": {
    //             "text": r"${text}",
    //             "style": {"fontSize": r"${fontSize}", "color": r"${color}"}
    //           }
    //         }
    //       },
    //       {
    //         "type": "expanded",
    //         "child": {
    //           "type": "dropdown_button_form_field",
    //           "args": {
    //             "isExpanded": true,
    //             "decoration": {"labelText": "Email Type"},
    //             "items": ["Home", "Other", "Work"],
    //             "validators": [
    //               {"type": "required"}
    //             ]
    //           }
    //         }
    //       }
    //     ]
    //   }
    // };
    this.widgetJsonData = widgetJsonData ?? jsonData;
  }
  //final a = DropdownButtonFormField()
  Map<String, num> fontSizeLabelsMap = {
    "小": 10.0,
    "中": 14.0,
    "大": 20.0,
  };
  int getFontSizeIndex() {
    return fontSizeLabelsMap.values.toList().indexOf(fontSize ?? 14);
  }

  setFontSize(int? index, ElementController controller) {
    fontSize = fontSizeLabelsMap.values.toList()[index ?? 1];
    controller.elementRegistry
        .setValue("fontSize", fontSizeLabelsMap.values.toList()[index ?? 1]);
    controller.update();
  }
}

class TextWidget extends ElementWidget {
  TextWidget(
      {super.key,
      required super.workController,
      widgetParams,
      super.parentElementController}) {
    elementController.widgetParams = widgetParams;
    elementController.type = 'text';
    elementRegistry.setValue("name", null);
    elementRegistry.setValue("valueKey", "text");
    elementRegistry.setValue("width", widgetParams.width);
    elementRegistry.setValue("height", widgetParams.height);
    elementRegistry.setValue("text", widgetParams.text);
    elementRegistry.setValue("fontSize", widgetParams.fontSize);
    elementRegistry.setValue("color", widgetParams.color);
  }
  @override
  Widget build(BuildContext context) {
    var main = JsonWidgetData.fromDynamic(
            elementController.widgetParams.widgetJsonData,
            registry: elementRegistry)!
        .build(context: context);

    return elementController.workController.mode == 'design'
        ? ElementActionWidget(
            elementController: elementController,
            child: GestureDetector(
                onDoubleTapDown: (details) {
                  doubletapDetails = details.localPosition;
                },
                onDoubleTap: () {
                  Offset position = doubletapDetails;
                  _displayMenu(
                      context, position, elementController, elementRegistry);
                },
                //child: elementController.child!,
                child: Container(
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.red)),
                    child: main)),
          )
        : main;
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  // StarMenuOverlay.displayStarMenu(
  //   context,
  //   StarMenu(
  //     parentContext: context,
  //     params: getMenuParams(
  //       context,
  //       onHoverScale: 1,
  //       centerOffset: position + const Offset(150, 150),
  //     ),
  //     onItemTapped: (index, controller) {
  //       if (index != 0) {
  //         controller.closeMenu!();
  //       }
  //     },
  //     items: [
  Get.defaultDialog(
      title: '设置text',
      titleStyle: const TextStyle(fontSize: 18),
      content: SizedBox(
        width: 240,
        height: 350,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InputWidget(
                  //width: 250,
                  //height: 150,
                  initText: registry.getValue("name"),
                  titleText: "名称:",
                  //hintText: "name",
                  onChanged: (value) {
                    if (value == "") value = "name";
                    registry.setValue("name", value);
                  }),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InputWidget(
                  //width: 250,
                  //height: 150,
                  initText: registry.getValue('text'),
                  titleText: "输入文本",
                  hintText: "Text",
                  onChanged: (value) {
                    if (value == "") value = "";
                    registry.setValue("text", value);
                  }),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ColorPicker(
                //color: workController.bgColor,
                title: const Text("字体颜色:"),
                width: 15,
                height: 15,
                padding: const EdgeInsets.all(2),
                pickersEnabled: const <ColorPickerType, bool>{
                  ColorPickerType.custom: true,
                  ColorPickerType.primary: false,
                  ColorPickerType.accent: false,
                },
                onColorChanged: (Color value) {
                  registry.setValue("color", value.hex);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ToggleWidget(
                titleWidget:
                    const Text("字体大小:", style: TextStyle(fontSize: 14)),
                initialLabelIndex:
                    elementController.widgetParams.getFontSizeIndex(),
                labels: elementController.widgetParams.fontSizeLabelsMap.keys
                    .toList(),
                labelsTextStyle: const [
                  TextStyle(fontSize: 10),
                  TextStyle(fontSize: 14),
                  TextStyle(fontSize: 20)
                ],
                onToggle: (index) {
                  elementController.widgetParams
                      .setFontSize(index, elementController);
                },
              ),
            ),
          ],
        ),
      ));
}
