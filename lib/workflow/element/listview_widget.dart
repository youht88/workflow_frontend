import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/tools/input_widget.dart';

import 'element_widget.dart';

class ListViewWidgetParams {
  Map<String, dynamic>? widgetJsonData;
  late Map<String, dynamic> widgetArgs;
  Map<String, dynamic> jsonData = {
    "type": "container",
    "args": {
      "width": 100,
      "height": 100,
    },
    "child": {
      "type": "list_view",
      // "args": {
      //   "scrollDirection": ThemeEncoder.encodeAxis(Axis.horizontal)
      // },
      "children": r"${for_each(data,'template')}"
    }
  };
  ListViewWidgetParams({widgetArgs, widgetJsonData}) {
    this.widgetArgs = widgetArgs ?? {};
    this.widgetJsonData = widgetJsonData ?? jsonData;
  }
}

class ListViewWidget extends ElementWidget {
  Widget layoutHandle = const Icon(Icons.list);
  ListViewWidget(
      {super.key,
      required super.workController,
      widgetParams,
      super.parentElementController}) {
    elementController.type = 'listview';
    elementController.layoutType = 'child';
    elementController.widgetParams = widgetParams;
    elementRegistry.setValue("name", null);
    elementRegistry.setValue("valueKey", "data");
    // elementRegistry.setValue("data", [
    //   {"a": "123", "b": "999"},
    //   {"a": "xyz", "b": "abc"}
    // ]);
    // elementRegistry.setValue("template", {
    //   "type": "text",
    //   "args": {"text": r"${value['b']}"}
    // });
  }
  @override
  Widget build(BuildContext context) {
    if (elementController.workController.mode == 'design') {
      return ElementActionWidget(
          elementController: elementController,
          child: GestureDetector(
            onDoubleTapDown: (details) {
              doubletapDetails = details.localPosition;
            },
            onDoubleTap: () {
              Offset position = doubletapDetails;
              _displayDialog(
                  context, position, elementController, elementRegistry);
            },
            child: GetBuilder<ElementController>(
              init: elementController,
              global: false,
              builder: (_) {
                return Row(
                  children: [
                    layoutHandle,
                    Container(
                      decoration:
                          BoxDecoration(border: Border.all(color: Colors.red)),
                      child: Container(child: elementController.child),
                    )
                  ],
                );
              },
            ),
          ));
    } else {
      debugPrint("${elementController.elementRegistry.values}");
      // elementController.widgetParams.widgetJsonData = {
      //   "type": "set_value",
      //   "args": elementController.elementRegistry.values,
      //   "child": elementController.widgetParams.widgetJsonData
      // };
      debugPrint("${elementController.widgetParams.widgetJsonData}");
      // return JsonWidgetData.fromDynamic(
      //         elementController.widgetParams.widgetJsonData,
      //         registry: elementController.elementRegistry)!
      //     .build(context: context);
      return ListView(children: [...elementController.children]);
      //children: [...elementController.children]);
    }
  }
}

_displayDialog(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  Get.defaultDialog(
      title: '设置listview',
      titleStyle: const TextStyle(fontSize: 18),
      content: SizedBox(
        width: 240,
        height: 350,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InputWidget(
                  //width: 250,
                  //height: 150,
                  initText: registry.getValue("name"),
                  titleText: "名称:",
                  //hintText: "name",
                  onChanged: (value) {
                    if (value == "") value = "name";
                    registry.setValue("name", value);
                  }),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InputWidget(
                  //width: 250,
                  //height: 150,
                  initText: "${registry.getValue('data')}",
                  titleText: "输入数据",
                  hintText: "[]",
                  onChanged: (value) {
                    if (value == "") value = "";
                    registry.setValue("data", value);
                  }),
            ),
          ],
        ),
      ));
}
