import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:star_menu/star_menu.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/menu/menu_params.dart';

import 'element_widget.dart';

class ExpandedWidgetParams {
  late Map<String, dynamic> widgetJsonData;
  ExpandedWidgetParams({child, widgetJsonData}) {
    String jsonData = r''' 
    {
      "type": "container",
      "args":{
        "width":"${width}",
        "height":"${height}",
        "padding":"${padding}",
        "alignment":"${alignment}"
      },
      "child":"${child}"
    }''';
    this.widgetJsonData = widgetJsonData ?? json.decode(jsonData);
  }
}

class ExpandedWidget extends ElementWidget {
  Widget layoutHandle = const Icon(Icons.expand);
  ExpandedWidget(
      {super.key,
      required super.workController,
      widgetParams,
      super.parentElementController}) {
    elementController.type = 'expanded';
    elementController.layoutType = 'child';
    elementController.widgetParams = widgetParams;
  }
  @override
  Widget build(BuildContext context) {
    Widget main = JsonWidgetData.fromDynamic(
            elementController.widgetParams.widgetJsonData,
            registry: elementRegistry)!
        .build(context: context);

    return ElementActionWidget(
      elementController: elementController,
      child: GestureDetector(
          onDoubleTapDown: (details) {
            doubletapDetails = details.localPosition;
          },
          onDoubleTap: () {
            Offset position = doubletapDetails;
            _displayMenu(context, position, elementController, elementRegistry);
          },
          //child: elementController.child!,
          child: GetBuilder<ElementController>(
            init: elementController,
            global: false,
            builder: (_) {
              return false
                  ? elementController.child!
                  : Row(children: [
                      layoutHandle,
                      Expanded(
                          child: elementController.child ?? const SizedBox()),
                    ]);
            },
          )),
    );
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  ExpandedWidgetParams widgetParams = elementController.widgetParams;
  Get.defaultDialog(
    title: '设置button',
    titleStyle: const TextStyle(fontSize: 18),
    content: const SizedBox(),
  );
}
