import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/tools/input_widget.dart';
import 'package:workflow/workflow/tools/multi_slider_widget.dart';

import 'element_widget.dart';

class NetworkImageWidgetParams {
  late Map<String, dynamic> widgetJsonData;
  double? width = 50;
  double? height = 50;
  String? src;
  NetworkImageWidgetParams({widgetJsonData, this.src}) {
    Map<String, dynamic> jsonData = {
      "type": "network_image",
      "args": {
        "fit": "cover",
        "width": r"${width}",
        "height": r"${height}",
        "src": r"${src}"
      }
    };
    this.widgetJsonData = widgetJsonData ?? jsonData;
  }
  setWidth(width, ElementController controller) {
    this.width = width == 0 ? null : width;
    controller.elementRegistry.setValue("width", width);
    controller.update();
  }

  setHeight(height, ElementController controller) {
    this.height = height == 0 ? null : height;
    controller.elementRegistry.setValue("height", height);
    controller.update();
  }
}

class NetworkImageWidget extends ElementWidget {
  NetworkImageWidget(
      {super.key,
      required super.workController,
      widgetParams,
      super.parentElementController}) {
    elementController.widgetParams = widgetParams;
    elementController.type = 'networkImage';
    elementRegistry.setValue("name", null);
    elementRegistry.setValue("valueKey", "src");
    elementRegistry.setValue("width", widgetParams.width);
    elementRegistry.setValue("height", widgetParams.height);
    elementRegistry.setValue("src",
        "https://plus.unsplash.com/premium_photo-1673126680027-73cb9bfb9529?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=387&q=80");
  }
  @override
  Widget build(BuildContext context) {
    final main = JsonWidgetData.fromDynamic(
            elementController.widgetParams.widgetJsonData,
            registry: elementRegistry)!
        .build(context: context);

    return (elementController.workController.mode == 'design')
        ? ElementActionWidget(
            elementController: elementController,
            child: GestureDetector(
                onDoubleTapDown: (details) {
                  doubletapDetails = details.localPosition;
                },
                onDoubleTap: () {
                  Offset position = doubletapDetails;
                  _displayMenu(
                      context, position, elementController, elementRegistry);
                },
                //child: elementController.child!,
                child: Container(
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.red)),
                    child: main)),
          )
        : main;
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  NetworkImageWidgetParams widgetParams = elementController.widgetParams;
  Get.defaultDialog(
    title: '设置网络图片',
    titleStyle: const TextStyle(fontSize: 18),
    content: SizedBox(
      width: 250,
      height: 350,
      child: SingleChildScrollView(
        child: Column(
          children: [
            InputWidget(
                //width: 250,
                //height: 150,
                initText: registry.getValue("name"),
                titleText: "名称:",
                //hintText: "name",
                onChanged: (value) {
                  if (value == "") value = "name";
                  registry.setValue("name", value);
                }),
            InputWidget(
              titleText: "图片地址:",
              initText: registry.getValue("src"),
              hintText: "输入URL地址",
              onSubmitted: (value) => registry.setValue("src", value),
            ),
            GetBuilder<ElementController>(
              init: elementController,
              global: false,
              builder: (_) {
                return MultiSliderWidget(
                  height: 120,
                  titleWidget: Text(
                      "尺寸: ${widgetParams.width?.toInt()}  ✖️  ${widgetParams.height?.toInt()}"),
                  labels: const ['宽度:', '高度:'],
                  labelsTextStyle: const [
                    TextStyle(fontSize: 12, color: Colors.black45),
                    TextStyle(fontSize: 12, color: Colors.black45)
                  ],
                  sliderMin: const [0, 0],
                  sliderMax: [
                    elementController.workController.width - 80,
                    elementController.workController.height - 80
                  ],
                  sliderValue: [
                    widgetParams.width ?? 0,
                    widgetParams.height ?? 0
                  ],
                  sliderOnChanged: [
                    (value) {
                      widgetParams.setWidth(value, elementController);
                    },
                    (value) {
                      widgetParams.setHeight(value, elementController);
                    }
                  ],
                );
              },
            ),
          ],
        ),
      ),
    ),
  );
}
