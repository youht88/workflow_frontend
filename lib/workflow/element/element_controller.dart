import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:workflow/workflow/work/work_controller.dart';

import 'element.dart';

class ElementController extends GetxController {
  List<ElementWidget> children = [];
  Widget? child;
  late WorkController workController;
  //late Map<String, dynamic> widgetJsonData;
  late ElementWidget elementWidget;
  late String type;
  String? layoutType;
  ElementController? parentElementController;
  late JsonWidgetRegistry elementRegistry;
  dynamic widgetParams;
  //每个组件setValue(value)存储本组件的值
  dynamic _value;
  dynamic get value => _value;
  set setValue(dynamic value) => _value = value;

  @override
  onInit() {
    super.onInit();
  }

  setupRegistry(Map<String, dynamic> registryMap) {
    for (var item in registryMap.entries) {
      elementRegistry.setValue(item.key, item.value);
    }
  }

  setupArgs(Map<String, dynamic>? widgetArgs) {
    widgetParams.widgetArgs = widgetArgs ?? {};
    update();
  }

  //将源组件elementWidget拖到本组件
  pushElement(ElementWidget elementWidget, workController) {
    if (layoutType == null) return;
    elementWidget.elementController.parentElementController = this;
    if (layoutType == 'children') {
      children.add(elementWidget);
    } else if (layoutType == 'child') {
      child = elementWidget;
    }
    update();
  }

  //在本布局组件中新增一个指定type的组件
  ElementWidget? addElement(
      {required String type,
      dynamic widgetParams,
      required WorkController workController,
      String mode = "design"}) {
    ElementWidget? elementWidget;
    switch (type) {
      case 'text':
        elementWidget = TextWidget(
            widgetParams: widgetParams ?? TextWidgetParams(),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
        break;
      case 'icon':
        elementWidget = IconWidget(
            widgetParams: IconWidgetParams(),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
        break;
      case 'network_image':
        elementWidget = NetworkImageWidget(
            widgetParams: NetworkImageWidgetParams(),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
        break;
      case 'elevated_button':
        elementWidget = ButtonWidget(
            widgetParams: ButtonWidgetParams(),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
        break;
      case 'custom':
        elementWidget = ElementWidget(
            widgetParams: ElementWidgetParams(),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
        break;
      case 'container':
        // child = ContainerWidget(
        //     containerWidgetParams: ContainerWidgetParams(),
        //     workController: workController,
        //     parentElementController: this);
        // break;
        elementWidget = ContainerWidget(
            widgetParams:
                ContainerWidgetParams(widgetArgs: widgetParams?['widgetArgs']),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
      case 'listview':
        elementWidget = ListViewWidget(
            widgetParams:
                ListViewWidgetParams(widgetArgs: widgetParams?['widgetArgs']),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
      case 'expanded':
        elementWidget = ExpandedWidget(
            widgetParams: ExpandedWidgetParams(),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
      case 'row':
        elementWidget = RowWidget(
          widgetParams:
              RowWidgetParams(widgetArgs: widgetParams?['widgetArgs']),
          workController: workController,
          parentElementController: this,
        );
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
        break;
      case 'column':
        elementWidget = ColumnWidget(
            widgetParams:
                ColumnWidgetParams(widgetArgs: widgetParams?['widgetArgs']),
            workController: workController,
            parentElementController: this);
        if (layoutType == 'children') {
          children.add(elementWidget);
        } else {
          child = elementWidget;
        }
        break;
      default:
        throw ("增加的组件尚未实现!");
    }

    update();
    return elementWidget;
  }

  ElementWidget? findElement(ElementController elementController) {
    if (layoutType == 'children') {
      try {
        return children
            .firstWhere((item) => item == elementController.elementWidget);
      } catch (e) {
        return null;
      }
    } else {
      try {
        return child as ElementWidget;
      } catch (e) {
        return null;
      }
    }
  }

  removeElement(ElementController elementController) {
    if (layoutType == 'children') {
      children.removeWhere((item) => item == elementController.elementWidget);
    } else {
      child = const SizedBox();
    }
    update();
  }
}
