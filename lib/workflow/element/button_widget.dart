import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/tools/input_widget.dart';

import 'element_widget.dart';

class ButtonWidgetParams {
  Widget child;
  late Map<String, dynamic> widgetJsonData;
  double? width = 50;
  double? height = 50;
  String? text = 'Button';
  List<double>? padding = [0, 0, 0, 0];
  String? style = "primary";
  String? onPressed = r"${valueOf()}";
  ButtonWidgetParams(
      {this.child = const SizedBox(),
      widgetJsonData,
      this.text = 'Button',
      this.style,
      this.onPressed}) {
    Map<String, dynamic> jsonData = {
      "type": "elevated_button",
      "args": {"style": r"${style}"},
      "child": {
        "type": "text",
        "args": {"text": r"${text}"}
      }
    };
    this.widgetJsonData = widgetJsonData ?? jsonData;
  }
}

class ButtonWidget extends ElementWidget {
  ButtonWidget({
    super.key,
    required super.workController,
    widgetParams,
    super.parentElementController,
  }) {
    elementController.widgetParams = widgetParams;
    elementController.type = 'elevated_button';
    elementRegistry.setValue("name", null);
    elementRegistry.setValue("valueKey", "onPressed");
    elementRegistry.setValue("width", widgetParams.width);
    elementRegistry.setValue("height", widgetParams.height);
    elementRegistry.setValue("style", widgetParams.style);
    elementRegistry.setValue("text", widgetParams.text);
  }
  @override
  Widget build(BuildContext context) {
    if (elementController.workController.mode != 'design') {
      String? onPressedStr = elementRegistry.getValue('onPressed');
      if ((onPressedStr ?? "") != "") {
        elementController.widgetParams.widgetJsonData['args']['onPressed'] =
            onPressedStr;
      } else {
        (elementController.widgetParams.widgetJsonData['args'] as Map)
            .remove('onPressed');
      }
    }

    final main = JsonWidgetData.fromDynamic(
            elementController.widgetParams.widgetJsonData,
            registry: elementRegistry)!
        .build(context: context);

    if (elementController.workController.mode == 'design') {
      return ElementActionWidget(
        elementController: elementController,
        child: GestureDetector(
            onDoubleTapDown: (details) {
              doubletapDetails = details.localPosition;
            },
            onDoubleTap: () {
              Offset position = doubletapDetails;
              _displayMenu(
                  context, position, elementController, elementRegistry);
            },
            //child: elementController.child!,
            child: Container(
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.red)),
                child: main)),
      );
    } else {
      return main;
    }
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  ButtonWidgetParams widgetParams = elementController.widgetParams;
  Get.defaultDialog(
      title: '设置button',
      titleStyle: const TextStyle(fontSize: 18),
      content: SizedBox(
        width: 250,
        height: 350,
        child: SingleChildScrollView(
          child: Column(
            children: [
              InputWidget(
                  titleText: "按钮文字:",
                  initText: registry.getValue("text"),
                  hintText: "输入按钮文字",
                  onChanged: (value) {
                    if (value == "") value = "Button";
                    registry.setValue("text", value);
                  }),
              InputWidget(
                  titleText: "点击动作:",
                  initText: registry.getValue("onPressed"),
                  hintText: "输入点击动作函数",
                  onSubmitted: (value) {
                    registry.setValue("onPressed", value);
                  }),
            ],
          ),
        ),
      ));
}
