import 'package:flutter/material.dart';

class BottomInputWidget extends StatelessWidget {
  late TextEditingController editController;
  String? initText;
  String? titleText;
  String? infoText;
  String? hintText;
  Function(String)? onChanged;
  Function()? onCancel;
  Function(String)? onSubmitted;
  BottomInputWidget(
      {super.key,
      this.initText,
      this.titleText,
      this.infoText,
      this.hintText,
      this.onChanged,
      this.onCancel,
      this.onSubmitted}) {
    assert((onChanged == null && onSubmitted != null) ||
        (onChanged != null && onSubmitted == null));
    editController = TextEditingController(text: initText);
    if (onSubmitted != null) {
      onChanged = (value) {
        editController.text = value;
      };
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            if (titleText != null)
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 4.0),
                child: Text(titleText!,
                    style: const TextStyle(fontWeight: FontWeight.bold)),
              ),
            if (infoText != null)
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: Text(infoText!,
                    style:
                        const TextStyle(fontSize: 12, color: Colors.black45)),
              ),
            (onSubmitted != null)
                ? TextField(
                    minLines: 2,
                    maxLines: 2,
                    onTap: () {
                      debugPrint("?? onTap");
                    },
                    style: const TextStyle(fontSize: 14),
                    controller: editController,
                    decoration: InputDecoration(
                      hintText: hintText,
                      border: const OutlineInputBorder(),
                    ),
                    onSubmitted: onSubmitted)
                : TextField(
                    minLines: 1,
                    maxLines: 4,
                    style: const TextStyle(fontSize: 14),
                    controller: editController,
                    decoration: InputDecoration(
                      hintText: hintText,
                      border: const OutlineInputBorder(),
                    ),
                    onChanged: onChanged,
                  )
          ],
        ),
        if (onSubmitted != null)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton.icon(
                onPressed: () {
                  onSubmitted!(editController.text);
                },
                icon: const Icon(Icons.check_circle),
                label: const Text("确认")),
          )
      ],
    );
  }
}
