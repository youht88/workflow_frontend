import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BottomConfirmWidget extends StatelessWidget {
  String title;
  String content;
  Widget? cancelWidget;
  Widget? doWidget;
  BottomConfirmWidget(
      {super.key,
      required this.title,
      required this.content,
      cancelWidget,
      doWidget}) {
    this.cancelWidget = cancelWidget ??
        Padding(
            padding: const EdgeInsets.all(8),
            child: ElevatedButton.icon(
                onPressed: () {
                  Get.back(result: false);
                },
                icon: const Icon(Icons.cancel),
                label: const Text("取消")));
    this.doWidget = doWidget ??
        Padding(
            padding: const EdgeInsets.all(8),
            child: ElevatedButton.icon(
                onPressed: () {
                  Get.back(result: true);
                },
                icon: const Icon(Icons.confirmation_num),
                label: const Text("确认")));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(title,
                style:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            Text(content, style: const TextStyle(fontSize: 14)),
            Row(children: [
              Expanded(flex: 1, child: cancelWidget!),
              Expanded(flex: 1, child: doWidget!)
            ])
          ]),
    ));
  }
}
