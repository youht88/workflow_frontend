import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';

class ColorPickerWidget extends StatelessWidget {
  double? width;
  double? height;
  Color initColor;
  Widget? titleWidget;
  void Function(Color) onColorChange;
  ColorPickerWidget(
      {super.key,
      this.width,
      this.height,
      this.titleWidget,
      required this.initColor,
      required this.onColorChange});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
          width: width,
          height: height,
          child: ColorPicker(
              color: initColor,
              title: titleWidget,
              width: 15,
              height: 15,
              padding: const EdgeInsets.all(2),
              pickersEnabled: const <ColorPickerType, bool>{
                ColorPickerType.custom: false,
                ColorPickerType.primary: true,
                ColorPickerType.accent: false,
                ColorPickerType.bw: false,
                ColorPickerType.both: false,
                ColorPickerType.wheel: false,
              },
              onColorChanged: onColorChange)),
    );
  }
}
