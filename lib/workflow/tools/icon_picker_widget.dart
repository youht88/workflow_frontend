import 'package:flutter/material.dart';
import 'package:workflow/util/data_util/icon_lib.dart';

class IconPickerWidget extends StatelessWidget {
  double? width;
  double height;
  IconData? initIcon;
  int crossAxisCount;
  double size;
  Widget? titleWidget;
  void Function(IconData) onIconChange;
  IconPickerWidget(
      {super.key,
      this.width,
      required this.height,
      this.size = 20.0,
      this.crossAxisCount = 8,
      this.titleWidget,
      this.initIcon,
      required this.onIconChange});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: height,
        child: GridView.count(
          crossAxisCount: crossAxisCount,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          shrinkWrap: true,
          children: IconsMap.entries
              .map((item) => GestureDetector(
                  onTap: () {
                    debugPrint("current icon name:${item.key}");
                    onIconChange(item.value);
                  },
                  child: Icon(item.value, size: size)))
              .toList(),
        ),
      ),
    );
  }
}
