import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/data/constant.dart';
import 'package:workflow/util/cache/cache_lib.dart';
import 'package:workflow/util/crypto_util/crypto_lib.dart';
import 'package:workflow/util/network/http_util.dart';

class MyProfile {
  bool isFirstLogin = true;
  bool designMode = false;
  Map<String, dynamic> accountMap = {};
  String name = "user1";
  String email = "email1";
  String avatar =
      "https://pic1.zhimg.com/v2-2fde995608daf5801b8f1189bbbdc78b_r.jpg?source=1940ef5c";
  String endpoint = Constant.endpoint;
  String systemMessage =
      "请尽可能使用中文回复,在文字中多使用一些图标字体。所有单位默认采用公制单位，不要使用英制单位。如果我问你“某个人是谁”或“某公司是什么公司”这类你并不确信的问题，请回答使用搜索引擎。";
  bool useAction = false;
  String lastConversationId = "";
  Future<void> loadStorage() async {
    await Storage.init();
    //await Storage.reset();
    isFirstLogin = Storage.get('firstLogin') ?? true;
    if (isFirstLogin) {
      await firstLogin();
    } else {
      accountMap = Storage.get('accountMap');
      await getUserProfile(accountMap);
//      final sig = CryptoLib.sign("abcd", userAccount["privateKey"],
//          algType: CryptoAlgorithm.ECC, hashType: HashAlgorithm.SHA256);
//      debugPrint(
//          "${CryptoLib.verify("abcd", userAccount["publicKey"], sig, algType: CryptoAlgorithm.ECC, hashType: HashAlgorithm.SHA256)}");
    }
    return;
  }

  Future firstLogin() async {
    final accountMap = CryptoLib.genSM2KeyPair();
    await Storage.set("firstLogin", false);
    await setUserProfile(accountMap);
  }

  Future setUserProfile(Map<String, dynamic> accountMap) async {
    const url = Constant.setUserProfile;
    await Storage.set("accountMap", accountMap);
    this.accountMap = accountMap;
    debugPrint("$accountMap");

    designMode = Storage.get("designMode") ?? designMode;
    name = Storage.get("name") ?? name;
    email = Storage.get("email") ?? email;
    avatar = Storage.get("avatar") ?? avatar;
    systemMessage = Storage.get("systemMessage") ?? systemMessage;
    endpoint = Storage.get("endpoint") ?? endpoint;
    useAction = Storage.get("useAction") ?? useAction;
    final data = {
      "account": accountMap['account'],
      "publicKey": accountMap['publicKey'],
      "designMode": designMode,
      "name": name,
      "email": email,
      "avatar": avatar,
      "systemMessage": systemMessage,
      "endpoint": endpoint,
      "useAction": useAction,
      "conversationTitles": []
    };
    await HttpLib.postData(url, body: data);
  }

  Future updateUserProfile(String account, Map<String, dynamic> data) async {
    const url = Constant.updateUserProfile;
    final query = {"account": account};
    for (final item in data.entries) {
      if (item.key != "account") {
        await Storage.set(item.key, item.value);
      }
    }
    await HttpLib.postData(url, query: query, body: data);
  }

  Future getUserProfile(accountMap) async {
    const url = Constant.getUserProfile;
    final query = {"account": accountMap['account']};
    try {
      final response = await HttpLib.getData(url, query: query);
      final result = json.decode(json.encode(response))[0];
      designMode = result['designMode'];
      name = result["name"];
      email = result["email"];
      avatar = result["avatar"];
      systemMessage = result["systemMessage"];
      endpoint = result["endpoint"];
      useAction = result["useAction"];
      lastConversationId = result["currentConversationId"] ?? "";
    } catch (e) {
      debugPrint("error on getUserProfile:$e");
      await Get.defaultDialog(content: const Text("网络错误,请稍后再试。"));
      exit(0);
      //setUserProfile(accountMap);
    }
  }
}
