import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/project/project_controller.dart';
import 'package:workflow/workflow/tools/input_widget.dart';

displayProjectMenu(BuildContext context, ProjectController projectController) {
  // StarMenuOverlay.displayStarMenu(
  //   context,
  //   StarMenu(
  //     params: getMenuParams(context),
  //     onItemTapped: (index, controller) {
  //       if (index == 0 || index >= 4) {
  //         controller.closeMenu!();
  //       }
  //     },
  //     items: [
  //
  Get.defaultDialog(
      title: "设置project",
      titleStyle: const TextStyle(fontSize: 18),
      content: SizedBox(
        width: 300,
        height: 350,
        child: ListView(children: [
          InkWell(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text("ID:",
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text(projectController.projectId,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 10))
              ],
            ),
          ),
          const SizedBox(height: 8),
          InputWidget(
              titleText: "名称:",
              initText: projectController.projectName,
              hintText: "输入工程模块的名字",
              onChanged: (name) {
                projectController.setProjectName(name);
              }),
          const SizedBox(height: 8),
          const Divider(),
          InkWell(
            onTap: () {
              Get.back();
              projectController.exportRemote();
            },
            child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.save), Text('保存')]),
          ),
          if (projectController.workflowController.projectTabs.length > 1)
            InkWell(
              onTap: () {
                Get.back();
                projectController.removeRemote();
              },
              child: const Row(mainAxisSize: MainAxisSize.min, children: [
                Icon(Icons.delete, color: Colors.red),
                Text('删除')
              ]),
            ),
        ]),
      ));
}
