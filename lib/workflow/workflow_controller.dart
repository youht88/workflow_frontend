import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logging/logging.dart';
import 'package:tabbed_view/tabbed_view.dart';
import 'package:workflow/data/constant.dart';
import 'package:workflow/util/network/http_util.dart';
import 'package:workflow/workflow/chat/chat_page.dart';
import 'package:workflow/workflow/client/client.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/flow/flow_controller.dart';
import 'package:workflow/workflow/menu/project_menu.dart';
import 'package:workflow/workflow/myprofile.dart';
import 'package:workflow/workflow/project/project_controller.dart';
import 'package:workflow/workflow/project/project_page.dart';
import 'package:workflow/workflow/work/work_page.dart';

import 'grid_background.dart';
import 'work/work.dart';

class WorkflowController extends GetxController {
  late RpcClient rpcWorkflowClient;
  late RpcClient rpcUserClient;
  late TabbedViewController projectTabController;
  List<TabData> projectTabs = [];
  //List<ProjectWidget> projects = [];
  List<ProjectWidget> get projectWidgets =>
      projectTabs.map<ProjectWidget>((item) => item.value).toList();
  ProjectWidget? currentProjectWidget;
  MyProfile myProfile = MyProfile();
  @override
  onInit() async {
    super.onInit();
    Get.create(() => ProjectController());
    Get.create(() => WorkController());
    Get.create(() => FlowController());
    Get.create(() => ElementController());
    projectTabController =
        TabbedViewController(projectTabs, reorderEnable: false);

    Logger.root.level = Level.FINEST;
    Logger.root.onRecord.listen((record) {
      if (record.loggerName == 'log') {
        debugPrint(
            '${record.level.name}: ${record.loggerName}: ${record.time}: ${record.message}');
      }
    });
    // rpcWorkflowClient = RpcClient();
    // rpcWorkflowClient.init();
    // await rpcWorkflowClient.getInfo();

    // rpcUserClient = RpcClient(host: "localhost", port: 3344);
    // rpcUserClient.init();
    // await rpcUserClient.createUser();
    // await rpcUserClient.upload();
    // await rpcUserClient.download();
    // await rpcUserClient.chat();
  }

  @override
  onClose() async {
    super.onClose();
    await rpcWorkflowClient.close();
    await rpcUserClient.close();
  }

  @override
  onReady() async {
    //load storage
    await myProfile.loadStorage();
    final account = myProfile.accountMap['account'];
    List projectData = [];
    try {
      projectData = await HttpLib.getData(Constant.importProject,
          query: {"account": account});
    } catch (e) {
      debugPrint("importProject error:$e");
    }
    if (projectData.isEmpty) {
      addProjectWidget('project1');
    } else {
      for (final project in projectData) {
        final projectWidget =
            addProjectWidget(project['projectName'], project['projectId']);
        await projectWidget.projectController
            .importRemote(json.decode(project['content']));
      }
    }
    await chat();
  }

  chat() async {
    await Get.to(ChatWidget());
  }

  List<String> getProjectNames() {
    return projectWidgets
        .map((item) => item.projectController.projectName)
        .toList();
  }

  List<List<WorkWidget>> getProjectWorkWidgets() {
    return projectWidgets.map((item) => item.projectController.works).toList();
  }

  ProjectWidget addProjectWidget(projectName, [projectId]) {
    ProjectWidget projectWidget = ProjectWidget(
        projectName: projectName, projectId: projectId, mode: 'design');
    projectTabController.addTab(TabData(
        value: projectWidget,
        text: projectName,
        leading: (context, status) {
          return InkWell(
            onTap: () {
              displayProjectMenu(context, projectWidget.projectController);
            },
            child: const Icon(Icons.note_alt, size: 16),
          );
        },
        content: Stack(children: [
          const Positioned.fill(
            child: GridBackground(params: GridBackgroundParams()),
          ),
          projectWidget
        ])));
    currentProjectWidget = projectWidget;
    update();
    return projectWidget;
  }

  ProjectWidget? selectProjectWidget(int index) {
    currentProjectWidget = projectTabs[index].value;
    update();
    return currentProjectWidget;
  }

  removeProjectWidget(int index) {
    if (projectTabs.length <= 1) {
      Get.snackbar("⚠️警告", "至少应保留一个工程");
      return;
    }
    projectTabs.removeAt(index);
    if (projectTabs.isNotEmpty) {
      currentProjectWidget = selectProjectWidget(0);
    } else {
      currentProjectWidget = null;
    }
    update();
  }
}
