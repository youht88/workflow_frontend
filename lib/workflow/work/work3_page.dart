import 'package:flutter/material.dart';
import 'package:workflow/workflow/flow/flow_page.dart';
import 'package:workflow/workflow/work/work_page.dart';

class Work3Widget extends WorkWidget {
  Work3Widget(
      {super.key, super.id, super.targetIds, required super.projectController});
  @override
  Widget build(BuildContext context) {
    return FlowWidget(
      id: workController.id,
      targetIds: super.targetIds,
      workController: workController,
      width: width,
      height: height,
      child: WorkTabWidget(
          workController: workController, width: width, height: height),
    );
  }
}
